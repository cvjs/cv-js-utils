var WebpackUtils = {
  //getTimeStr()是获取当前时间的格式化字符串，如：20200107211212
  getTimeStr() {
    let date = new Date();
    let Y = date.getFullYear();
    let M = date.getMonth();
    M = M + 1 < 10 ? '0' + (M + 1) : M + 1;
    let D = date.getDate();
    D = D < 10 ? '0' + D : D;
    let h = date.getHours();
    h = h < 10 ? '0' + h : h;
    let m = date.getMinutes();
    m = m < 10 ? '0' + m : m;
    let s = date.getSeconds();
    //比如需要这样的格式 yyyy-MM-dd hh:mm:ss
    //console.log(Y + '-' + M + '-' + D + ' ' + h + ':' + m + ':' + s);
    let yyyyMMddhhmm = Y + '-' + M + D + '-' + h + m;
    return yyyyMMddhhmm;
  },
  // 设置 oss 路径：应用 + 年月日时
  // 上述代码是修改你的资源根目录，改为你上传到oss上的目录
  joinPathStr(appsName, isDate = false) {
    let lastPathArr = [];
    appsName = appsName || '';
    if (appsName != '') {
      appsName = appsName.replace(/\/\//g, '/'); // 双斜杠改单斜杠
      appsName = appsName.replace(/\/\//g, '/'); // 双斜杠改单斜杠
      appsName = appsName.replace(/(^\/)|(\/$)/, ''); // 去掉开头结尾斜杠
      lastPathArr.push(appsName);
    }
    if (isDate) {
      let currDate = this.getTimeStr();
      lastPathArr.push(currDate);
    }
    return lastPathArr.join('/');
  }
};

module.exports = WebpackUtils;
