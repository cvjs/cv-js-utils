# cv-webpack-utils

手册参考：https://gitee.com/cvjs/cv-webpack-utils 

#### 介绍

- webpack(>=4)插件
- 打包上传到阿里云oss
- 上传资源到阿里云oss
- 上传 webpack 编译后的文件到阿里云 OSS ( with webpack )
- 可作为webpack插件使用 ( without webpack )
- 也可独立使用(从0.1.1开始支持)
- 默认按output.path (webpack.config.js) 下面的文件路径上传到oss，需要指定上传根目录(dist)。
- 也可以通过`setOssPath`来配置不同的上传路径。
- 独立使用时请通过`setOssPath`指定上传路径, 否则将上传到`dist`指定的路径下。


Install 安装依赖
------------------------
```shell
$ npm install @10yun/cv-webpack-utils -D
```


## Options配置参数说明

| 参数            | 类型               | 是否必填 | 说明                                                                   |
| :-------------- | :----------------- | :------- | :--------------------------------------------------------------------- |
| accessKeyId     | String             | 是       | 阿里云授权 accessKeyId                                                 |
| accessKeySecret | String             | 是       | 阿里云授权 accessKeySecret                                             |
| region          | String             | -        | bucket 所在的区域，如果是在阿里云机器上，可以使用内部 region，节省流量 |
| bucket          | String             | 是       | 需要上传到到哪个 bucket 的名称                                         |
| filter          | Function(filepath) | -        | 文件过滤器，通过该方法可自由决定哪些文件需要上传                       |
| timeout         | Number             | -        | oss超时设置，默认为30秒(30000)                                         |
| overwrite       | Boolean            | -        | 是否覆盖oss同名文件。默认true                                          |
| verbose         | Boolean            | -        | 是否显示上传日志，默认为true                                           |
| deletOrigin     | Boolean            | -        | 上传完成是否删除原文件，默认false                                      |


> 更多详解

- `from`: 上传哪些文件，支持类似gulp.src的glob方法，如'./build/**', 可以为glob字符串或者数组。
    - 作为插件使用时：可选，默认为output.path下所有的文件。
    - 独立使用时：必须，否则不知道从哪里取图片：）
- `dist`: 上传到oss哪个目录下，默认为oss根目录。可作为路径前缀使用。
- `region`: 阿里云上传区域
- `deleteEmptyDir`: 如果某个目录下的文件都上传到cdn了，是否删除此目录。deleteOrigin为true时候生效。默认false。
- `setOssPath`: 自定义上传路径的函数。接收参数为当前文件路径。不传，或者所传函数返回false则按默认路径上传。(默认为output.path下文件路径)
- `setHeaders`: 配置headers的函数。接收参数为当前文件路径。不传，或者所传函数返回false则不设置header。
- `buildRoot`: 构建目录名。如：build。独立使用时候需要。如果已传setOssPath可忽略。默认为空
- `test`: 测试，仅显示要上传的文件，但是不执行上传操作。默认false



> accessKeyId & accessKeySecret 保密

- 注意: `accessKeyId, accessKeySecret` 很重要，注意保密!!!
- 如果将 `accessKeyId` 和 `accessKeySecret` 直接写到代码中势必造成了安全隐患
- 为了安全起见，可以将敏感信息保存到编译机的配置文件中


## 使用说明 - 作为webpack插件使用

配置 webpack.config.js

```javascript

const dev = process.env.NODE_ENV === "development";

const WebpackAliyunOss = require('@10yun/cv-webpack-utils/aliyun-oss');

let proConfWebpackPulgins = [];
// 建议只在生产环境配置代码上传
if (!dev) {
  // 上传到阿里云
  proConfWebpackPulgins.push(
    new WebpackAliyunOss({
      from: ['./build/**', '!./build/**/*.html'], // build目录下除html之外的所有文件
      dist: '/path/in/alioss', // oss上传目录
      region: 'your region', // bucket所在区域的接入点，例如 oss-cn-hangzhou
      accessKeyId: 'your key',
      accessKeySecret: 'your secret',
      bucket: 'your bucket',

    // 如果希望自定义上传路径，就传这个函数
    // 否则按 output.path (webpack.config.js) 目录下的文件路径上传
      setOssPath(filePath) {
        // filePath为当前文件路径。函数应该返回路径+文件名。
	// 如果返回/new/path/to/file.js，则最终上传路径为 /path/in/alioss/new/path/to/file.js
        return '/new/path/to/file.js';
      },
      // 如果想定义header就传
      setHeaders(filePath) {
        // 定义当前文件header，可选
        return {
          'Cache-Control': 'max-age=31536000'
        }
      },
      // filter: function (asset) {
      //     return !/\.html$/.test(asset);
      // },
    })
  );
}

const webpackConfig = {
  // ... 省略其他
  plugins: proConfWebpackPulgins
}
  
```

## 使用说明 - 独立使用

```javascript

const WebpackAliyunOss = require('@10yun/cv-webpack-utils/aliyun-oss');
new WebpackAliyunOss({
    from: ['./build/**', '!./build/**/*.html'],
    dist: '/path/in/alioss',
    buildRoot: 'build', // 构建目录，如果已传setOssPath，可忽略
    region: 'your region',
    accessKeyId: 'your key',
    accessKeySecret: 'your secret',
    bucket: 'your bucket',

    // 如果希望自定义上传路径，就传这个函数
    // 否则按`buildRoot`下的文件结构上传
    setOssPath(filePath) {
      // filePath为当前文件路径。函数应该返回路径+文件名。
      // 如果返回/new/path/to/file.js，则最终上传路径为 /path/in/alioss/new/path/to/file.js
      return '/new/path/to/file.js';
    },

    // 如果想定义header就传
    setHeaders(filePath) {
      return {
        'Cache-Control': 'max-age=31536000'
      }
    }
}).apply(); 
```   



#### 感谢

- https://github.com/gp5251/webpack-aliyun-oss