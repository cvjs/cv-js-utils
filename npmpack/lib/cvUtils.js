function V() {
  for (var e = arguments.length || 0, t = [], r = 0; r < e; r++)
    t.push(...arguments[r]);
  return t;
}
function ee(e, t) {
  var r = Array.prototype.slice.call(arguments), n = 1, i, o = typeof r[r.length - 1] == "boolean" ? r.pop() : !0;
  for (r.length === 1 && (e = this, n = 0); t = r[n++]; )
    for (i in t)
      i in e ? typeof t[i] == "object" && typeof e[i] == "object" ? e[i] = Object.assign(e[i], t[i]) : typeof t[i] == "function" && typeof e[i] == "function" && (e[i] = t[i]) : (o || !(i in e)) && (e[i] = t[i]);
  return e;
}
function te(e) {
  for (var t = Array.prototype.slice.call(arguments, 1), r = 0; r < t.length; r += 1) {
    var n = t[r];
    for (var i in n)
      n.hasOwnProperty(i) && (e[i] = n[i]);
  }
  return e;
}
function re(e) {
  let t = !1;
  return typeof e == "object" && e.length > 0 && (t = e[e.length - 1]), t;
}
function ne(e, t, r) {
  var n = function(f) {
    return function(g, d) {
      var y = g[f], b = d[f];
      return y < b ? -1 : y > b ? 1 : 0;
    };
  }, i = e.sort(n(t));
  if (r = r || !1, r == !0) {
    for (var o = [], u = 0; u < 26; u++) {
      var a = String.fromCharCode(65 + u), c = [];
      for (let f in i)
        a === i[f][t] && i[f] !== void 0 && i[f] !== null && c.push(i[f]);
      c !== void 0 && c.length > 0 && o.push({
        letter: a,
        tree: c
      });
    }
    return o;
  } else
    return i;
}
function ie(e, t) {
  var r = [];
  return r = e, e.forEach(function(n, i) {
    for (var o in n) {
      (n[o] == "true" || n[o] == !0) && (r[i][o] = !0), (n[o] == "false" || n[o] == !1) && (r[i][o] = !1);
      for (var u in initArr)
        if (o == u)
          switch (initArr[u]) {
            case "[]":
              (n[o] == "" || n[o] == null) && (r[i][o] = []);
          }
    }
  }), r;
}
function oe(e, t) {
  for (s = 0; s < e.length; s++)
    if (thisEntry = e[s].toString(), thisEntry == t)
      return !0;
  return !1;
}
function se(e, t, r = !1) {
  return Array.isArray(t) ? r ? !!t.find((n) => n && n.indexOf("*") && new RegExp("^" + n.replace(/[-\/\\^$+?.()|[\]{}]/g, "\\$&").replace(/\*/g, ".*") + "$", "g").test(e) ? !0 : n == e) : t.includes(e) : !1;
}
function ue(e, t) {
  return e.includes(t);
}
function ae(e, t) {
  let r = [];
  return typeof t == "string" && t.constructor == String && (t = t.replace(/\s*/g, ""), t = t.split(",")), e.forEach(function(n, i) {
    r[i] = {}, t.map(function(o) {
      r[i][o] = "", n[o] !== void 0 && n[o] !== null && (r[i][o] = n[o]);
    });
  }), r;
}
function ce(e) {
  return Array.from(new Set(e));
}
function fe(e) {
  if (e)
    try {
      return e.length;
    } catch {
      return 0;
    }
  return 0;
}
function j(e = [], t = { id: "id", pid: "pid", children: "children" }) {
  let r = [], n = {}, i = [0, "0", void 0, "undefined", null, "null", "00000000-0000-0000-0000-000000000000", ""];
  return e.forEach((o) => {
    let u = e.filter((c) => c[t.pid] === o[t.id]);
    o[t.children] && o[t.children] instanceof Array && o[t.children].length > 0 ? (o[t.children].map((c) => n[c[t.id]] = 1), o[t.children].push(...u.filter((c) => n[c[t.id]] !== 1))) : o[t.children] = u;
    let a = u.length > 0;
    (a || !a && i.includes(o[t.pid])) && r.push(o);
  }), r.every((o) => i.includes(o[t.pid])) ? r : j(r, t);
}
function v(e = [], t = "children") {
  return e.reduce((r, n) => r.concat(n, n[t] ? v(n[t], t) : []), []);
}
function T(e, t = "") {
  return e.reduce((r, n) => r.concat(n[t] || [], n[t] ? T([n[t]], t) : []), []);
}
function le(e = []) {
  let t = 0;
  return function r(n, i) {
    ++i, t = Math.max(i, t);
    for (let o = 0; o < n.length; o++) {
      let u = n[o];
      u.level = i, u.children && u.children.length > 0 ? r(u.children, i) : delete u.children;
    }
  }(e, 0), t;
}
function D(e = [], t, r = "id", n = "children") {
  return e.reduce((i, o) => i.concat(o[r] == t ? o : D(o[n] || [], t, r, n)), []);
}
function _(e, t, r) {
  e[t] && (r && r(e[t]), _(e[t], t, r));
}
function de() {
  if (I()) return "edge";
  if (E()) return "opera";
  if (F()) return "firefox";
  if (N()) return "weixin";
  if ($()) return "safari";
  if (C()) return "chrome";
}
function he(e) {
  const t = e || navigator.userAgent;
  return t.indexOf("MSIE") >= 0 && t.indexOf("Opera") < 0 || t.indexOf("MSIE") > -1 && t.indexOf("compatible") > -1 || t.indexOf("MSIE") > -1 && t.indexOf("Trident") > -1;
}
function pe(e) {
  const t = e || navigator.userAgent;
  return t.indexOf("Trident") > -1 && t.indexOf("rv:11.0") > -1;
}
function I(e) {
  return (e || navigator.userAgent).indexOf("Edg") > -1;
}
function C(e) {
  return (e || navigator.userAgent).indexOf("Chrome") > -1;
}
function E(e) {
  const t = e || navigator.userAgent;
  return t.indexOf("Opera") > -1 || t.indexOf("OPR") > -1;
}
function F(e) {
  return (e || navigator.userAgent).indexOf("Firefox") > -1;
}
function $(e) {
  const t = e || navigator.userAgent;
  return t.indexOf("Safari") > -1 && t.indexOf("Chrome") == -1 && t.indexOf("MicroMessenger") == -1;
}
function ge(e) {
  return (e || navigator.userAgent).indexOf("AppleWebKit") > -1;
}
function N() {
  const e = window.navigator.userAgent.toLowerCase();
  return e.match(/MicroMessenger/i) == "micromessenger" || e.indexOf("MicroMessenger") > -1;
}
function be(e) {
  return (e || navigator.userAgent).match(/\sQQ/i) == " QQ";
}
function ye(e) {
  const t = e || navigator.userAgent;
  return !!/Weibo/i.test(t);
}
function l(e) {
  return e.replace(/\s+/g, "");
}
function me(e) {
  let t = /^[0-9]{12,}$/;
  return e = e ? l(e) : "", t.test(e);
}
function xe(e) {
  let t = /^[0-9]{6}$/;
  return e = e ? l(e) : "", t.test(e);
}
function Ae(e) {
  return /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(e);
}
function we(e) {
  return e = e || "", e == "" ? !1 : (e = e ? l(e) : "", e = parseInt(e), !!/^((13[0-9])|(14[5,7,9])|(15[^4])|(16[6])|(17[1,2,3,5,6,7,8])|(18[0-9])|(19[1,3,8,9]))+\d{8}$/.test(e));
}
function Se(e) {
  e = e ? l(e) : "";
  let t = /^([0-9]{3,4}-)?[0-9]{7,8}$/, r = /^0?1[3|4|5|8][0-9]\d{8}$/, n = /^400[0-9]{7}$/;
  return !!(r.test(e) || t.test(e) || n.test(e));
}
function Me(e) {
  return e = e ? l(e) : "", !!"/^http|https://([w-]+(.[w-]+)+(/[w-./?%@&+=一-龥]*)?)?$/".test(e);
}
function Oe(e) {
  return e = e ? l(e) : "", !!"/^(w+://)?w+(.w+)+.*$/".test(e);
}
function je(e) {
  return /[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF][\u200D|\uFE0F]|[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF]|[0-9|*|#]\uFE0F\u20E3|[0-9|#]\u20E3|[\u203C-\u3299]\uFE0F\u200D|[\u203C-\u3299]\uFE0F|[\u2122-\u2B55]|\u303D|[\A9|\AE]\u3030|\uA9|\uAE|\u3030/gi.test(e);
}
function ve(e) {
  return e == null || e == null || e == "";
}
function Te(e) {
  return e = e ? l(e) : "", e.replace(/\s+/g, "") != "";
}
function De(e) {
  return /^\s*$/g.test(e.replace(/^\s+|\s+$/g, ""));
}
function _e(e) {
  for (let t in e)
    return !1;
  return !0;
}
function m(e) {
  return !!(e !== null && e !== "null" && e !== void 0 && e !== "undefined" && e);
}
function Ie(e) {
  return !(e || typeof window < "u" && window.navigator.userAgent).match(
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
  );
}
function Ce(e) {
  return !!(e || navigator.userAgent).match(/AppleWebKit.*Mobile.*/);
}
function Ee(e) {
  const t = e || navigator.userAgent;
  return !!t.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/) || /iphone|ipad|ipod/i.test(t);
}
function Fe(e) {
  return (e || navigator.userAgent).indexOf("iPhone") > -1;
}
function $e(e) {
  return (e || navigator.userAgent).indexOf("iPad") > -1;
}
function Ne(e) {
  const t = e || navigator.userAgent;
  return t.indexOf("Android") > -1 || t.indexOf("Adr") > -1;
}
function Be() {
}
function Pe(e, t = 2) {
  var r = `/^(([1-9]{1}\\d*)|(0{1}))(\\.\\d{1,${t}})?$/`;
  return r.test(e);
}
function Le(e, t = 6) {
  var r = `/[0-9]{${t}}$/`;
  return r.test(e);
}
function Re(e) {
  return e.toString().search(/^[+-]?[0-9.]*$/) >= 0;
}
function ke(e) {
  return Math.floor(e) === e;
}
function qe(e) {
  if (e == "") return !1;
  var t = /^[1-9]\d*$/;
  return t.test(e);
}
function Ue(e) {
  var t = /^\d+(?=\.{0,1}\d+$|$)/;
  return !!t.test(e);
}
function Qe(e) {
  return /^[\u4e00-\u9fa5]+[\u00b7\.]?[\u4e00-\u9fa5]+$/.test(e);
}
function He(e) {
  return /[\u4e00-\u9fa5]+$/.test(e);
}
function Ge(e) {
  return /^[0-9a-zA-Z\u4e00-\u9fa5_-]+$/.test(e);
}
function We(e, t) {
  return e.charCodeAt(t) > 255 || e.charCodeAt(t) < 0;
}
function Ze(e) {
  return /^[a-zA-Z\u4e00-\u9fa5]+([\u00b7\.\- ]?[a-zA-Z\u4e00-\u9fa5]+)*$/.test(e);
}
function ze(e) {
  return /^[0-9A-Za-z]+$/.test(e);
}
function Ye(e) {
  return Object.prototype.toString.call(e) === "[object Array]";
}
function Je(e) {
  return Object.prototype.toString.call(e) === "[object Boolean]";
}
function Ke(e) {
  return Object.prototype.toString.call(e) === "[object Date]";
}
function Xe(e) {
  return Object.prototype.toString.call(e) === "[object Function]";
}
function Ve(e) {
  return Object.prototype.toString.call(e) === "[object Null]";
}
function et(e) {
  return Object.prototype.toString.call(e) === "[object Number]";
}
function tt(e) {
  return Object.prototype.toString.call(e) === "[object Object]";
}
function rt(e) {
  return Object.prototype.toString.call(e) === "[object String]";
}
function nt(e) {
  return Object.prototype.toString.call(e) === "[object Undefined]";
}
function it(e) {
  return Object.prototype.toString.call(e) === "[object RegExp]";
}
function ot(e) {
  try {
    JSON.parse(e);
  } catch {
    return !1;
  }
  return !0;
}
function st(e) {
  return typeof e == "object" && Object.prototype.toString.call(e).toLowerCase() == "[object object]" && typeof e.length > "u";
}
const B = function(e) {
  let t = "11:北京,12:天津,13:河北,14:山西,15:内蒙古,";
  t += "21:辽宁,22:吉林,23:黑龙江,", t += "31:上海,32:江苏,33:浙江,34:安徽,35:福建,36:江西,37:山东,", t += "41:河南,42:湖北,43:湖南,44:广东,45:广西,46:海南,", t += "50:重庆,51:四川,52:贵州,53:云南,54:西藏,", t += "61:陕西,62:甘肃,63:青海,64:宁夏,65:新疆,", t += "71:台湾,81:香港,82:澳门,91:国外";
  let r = t.split(","), n = {};
  for (let i in r) {
    let o = r[i].split(":");
    n[o[0]] = o[1];
  }
  return n[e.substr(0, 2)];
}, P = function(e) {
  return /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(e);
};
function L(e) {
  let t = e.split("");
  const r = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1], n = [1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2];
  let i = 0;
  t[17].toLowerCase() == "x" && (t[17] = 10);
  for (let u = 0; u < 17; u++)
    i += t[u] * r[u];
  let o = i % 11;
  return t[17] == n[o];
}
function R(e) {
  let t = e.substring(6, 10), r = e.substring(10, 12), n = e.substring(12, 14), i = new Date(t, parseFloat(r) - 1, parseFloat(n));
  return !(i.getFullYear() != parseFloat(t) || i.getMonth() != parseFloat(r) - 1 || i.getDate() != parseFloat(n));
}
function ut(e) {
  if (!e)
    return !1;
  e = (e + "").replace(/(^\s*)|(\s*$)/g, "");
  let t = e.match(/[^\x00-\xff]/g);
  return e.length + (t ? t.length : 0) != 18 ? !1 : (
    // 格式验证
    !!(P(e) && // 区域验证
    B(e) && // 生日验证
    R(e) && // 第18位的验证
    L(e))
  );
}
function at(e = "") {
  return e ? e.replace(/(^\s*)|(\s*$)/g, "") : "";
}
function ct(e) {
  return e ? e.replace(/\s|\xA0/g, "") : "";
}
function ft(e) {
  return typeof e == "number" || typeof e == "string" ? (e + "").length : 0;
}
function lt(e) {
  return e ? e.replace(/↵/g, "<br/>") : "";
}
function x(e, t, r = !1) {
  return e += "", t += "", r !== !0 && (e = e.toLowerCase(), t = t.toLowerCase()), e.indexOf(t) !== -1;
}
function k(e, t, r = !1) {
  return e += "", t += "", r !== !0 && (e = e.toLowerCase(), t = t.toLowerCase()), e.substring(0, t.length) === t;
}
function dt(e, t, r = !1) {
  return e += "", t += "", k(e, t, r) && (e = e.substring(t.length)), e || "";
}
function A(e, t, r = !1) {
  return e += "", t += "", r !== !0 && (e = e.toLowerCase(), t = t.toLowerCase()), e.substring(e.length - t.length) === t;
}
function q(e, t, r = !1) {
  return e += "", t += "", A(e, t, r) && (e = e.substring(0, e.length - t.length)), e || "";
}
function ht(e, t) {
  var r = e.length - t.length, n = e.substr(r, t.length);
  return n == t;
}
function pt(e, t, r) {
  return e ? e.replace(new RegExp(t, "g"), r) : "";
}
function gt(t) {
  var t = t.toString();
  return t.indexOf(".") != -1 ? t.replace(/(\d)(?=(\d{3})+\.)/g, function(r, n) {
    return n + ",";
  }) : t.replace(/(\d)(?=(\d{3}))/g, function(r, n) {
    return n + ",";
  });
}
function bt(e) {
  return e ? e.replace(/(\s)/g, "").replace(/(\d{4})/g, "$1 ").replace(/\s*$/, "") : "";
}
function yt(e) {
  return e ? e.replace(/\s/g, "").replace(/(\d{4})\d+(\d{4})$/, "**** **** **** $2") : "";
}
function mt(e) {
  return e ? e.replace(/^(\d{3})\d{4}(\d+)/, "$1****$2") : "";
}
function xt(e) {
  for (var t = new Array(), r = 0; r < e.length; r++)
    ;
  return e.charsArray = t, t;
}
function At(e = "") {
  return e.replace(/[\u4E00-\u9FA5]/g, "");
}
function wt(e, t, r) {
  return e += "", m(r) || (r = e.length), e.substring(t, r);
}
function U(e, t = null, r = null) {
  return e = e.toString(), m(t) && x(e, t) && (e = e.substring(e.indexOf(t) + t.length)), m(r) && x(e, r) && (e = e.substring(0, e.indexOf(r))), e;
}
function St(e = 0) {
  if (e = parseFloat(e), !isNaN(e)) {
    e = Math.round(e * 100);
    var t = e % 100 == 0, r = ["零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"], n = ["分", "角", "元", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟"], i = "", o, u, a, c = 0;
    if (e == 0)
      return "零元整";
    e < 0 && (i += "负", e = -e), e = e.toString(), a = e.length;
    for (var f = 0; f < a; f++)
      o = parseInt(e.charAt(f)), u = a - 1 - f, o == 0 ? u == 2 || u == 6 || u == 11 ? (i += n[u], c = 0) : c++ : (c > 0 && (i += "零", c = 0), i = i + r[o] + n[u]);
    return t && (i += "整"), i;
  }
}
function Mt(e) {
  return e == null ? null : (typeof e == "number" && (e = e.toString()), e.replace(/\B(?=(?:\d{3})+\b)/g, ","));
}
function Ot(e, t) {
  let r, n = null;
  return t = t || 500, function() {
    var i = arguments, o = +/* @__PURE__ */ new Date();
    r && o - r < t ? (clearTimeout(n), n = setTimeout(() => {
      r = o, e.apply(this, i);
    }, t)) : (r = o, e.apply(this, i));
  };
}
function jt(e, t) {
  var r = null;
  return t = t || 500, function() {
    var n = arguments;
    r !== null && clearTimeout(r), r = setTimeout(() => {
      r = null, e.apply(this, n);
    }, t);
  };
}
function Q(e, t = null) {
  let r = Number(e);
  if (r + "" == "NaN" && (r = 0), t && /^[0-9]*[1-9][0-9]*$/.test(t) && (r = r.toFixed(t), r.indexOf(".") < 0)) {
    r += ".";
    for (let i = 0; i < t; i++)
      r += "0";
  }
  return r;
}
function H(e) {
  var t = parseFloat(e[3] || 1), r = Math.floor(t * parseInt(e[0]) + (1 - t) * 255), n = Math.floor(t * parseInt(e[1]) + (1 - t) * 255), i = Math.floor(t * parseInt(e[2]) + (1 - t) * 255);
  return "#" + ("0" + r.toString(16)).slice(-2) + ("0" + n.toString(16)).slice(-2) + ("0" + i.toString(16)).slice(-2);
}
function vt() {
  let e = Math.floor(Math.random() * 256), t = Math.floor(Math.random() * 256), r = Math.floor(Math.random() * 256);
  return `rgb(${e},${t},${r})`;
}
function Tt(e, t, r) {
  for (var n = 1, i = 1, o = 1, u = e, a = 0; a < r; a++)
    for (var c = 0; c < t; c++)
      a == 0 ? (n += u[t * a + c], i += u[t * a + c + 1], o += u[t * a + c + 2]) : (n += u[(t * a + c) * 4], i += u[(t * a + c) * 4 + 1], o += u[(t * a + c) * 4 + 2]);
  n /= t * r, i /= t * r, o /= t * r, n = Math.round(n), i = Math.round(i), o = Math.round(o);
  let f = [Math.round(n), Math.round(i), Math.round(o)];
  return H(f);
}
function G(e) {
  var t, r;
  return e.indexOf("?") > -1 ? (t = e.split("?"), e = t[0]) : e.indexOf("#") > -1 && (t = e.split("#"), e = t[0]), t = e.split("/"), e = t[t.length - 1], t = e.lastIndexOf("."), r = t > -1 ? e.substr(t + 1).toLowerCase() : "", r;
}
function Dt(e) {
  var t = e == "" ? "" : G(e);
  return t == "jpg" || t == "jpeg" || t == "png" || t == "gif" || t == "bmp" ? 1 : 0;
}
function _t(e, t, r = "image/png") {
  return new Promise((n, i) => {
    let o = new Image();
    o.src = e, o.onload = function() {
      let u = this, a = u.width, c = u.height, f = a / c;
      c >= t.height && (c = t.height), a = c * f;
      let g = 1, d = document.createElement("canvas"), y = d.getContext("2d"), b = document.createAttribute("width");
      b.nodeValue = a;
      let w = document.createAttribute("height");
      w.nodeValue = c, d.setAttributeNode(b), d.setAttributeNode(w), y.drawImage(u, 0, 0, a, c), t.quality && t.quality <= 1 && t.quality > 0 && (g = t.quality);
      let O = d.toDataURL(r, g);
      n(O);
    }, o.onerror = () => {
      i();
    };
  });
}
function It(e) {
  if (e === 0) return "0 B";
  let t = 1024, r = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], n = Math.floor(Math.log(e) / Math.log(t));
  return typeof r[n] > "u" ? "0 B" : Q(e / Math.pow(t, n), 2) + " " + r[n];
}
function Ct(e, t, r, n, i = "") {
  if (e == "" || t == "") return "未知";
  var o = e * Math.PI / 180, u = r * Math.PI / 180, a = o - u, c = t * Math.PI / 180 - n * Math.PI / 180, f = 2 * Math.asin(
    Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(o) * Math.cos(u) * Math.pow(Math.sin(c / 2), 2))
  );
  switch (f = f * 6378.137, f = f.toFixed(2), i.toLowerCase()) {
    case "km":
      f = f + "km";
      break;
    case "m":
      f = f * 1e3, f = f < 100 ? "<100m" : f + "m";
      break;
    default:
      f > 1 ? f = f + "km" : (f = f * 1e3, f = f < 100 ? "<100m" : f + "m");
  }
  return f;
}
function Et(e, t, r) {
  e = e || 0, t = t || 0;
  var n, i, o, u;
  try {
    n = e.toString().split(".")[1].length;
  } catch {
    n = 0;
  }
  try {
    i = t.toString().split(".")[1].length;
  } catch {
    i = 0;
  }
  if (u = Math.abs(n - i), o = Math.pow(10, Math.max(n, i)), u > 0) {
    var a = Math.pow(10, u);
    n > i ? (e = Number(e.toString().replace(".", "")), t = Number(t.toString().replace(".", "")) * a) : (e = Number(e.toString().replace(".", "")) * a, t = Number(t.toString().replace(".", "")));
  } else
    e = Number(e.toString().replace(".", "")), t = Number(t.toString().replace(".", ""));
  return r || r === 0 ? ((e + t) / o).toFixed(r) : (e + t) / o;
}
function Ft(e, t, r) {
  e = e || 0, t = t || 0;
  var n, i, o, u;
  try {
    n = e.toString().split(".")[1].length;
  } catch {
    n = 0;
  }
  try {
    i = t.toString().split(".")[1].length;
  } catch {
    i = 0;
  }
  return o = Math.pow(10, Math.max(n, i)), u = n >= i ? n : i, r || r === 0 ? ((e * o - t * o) / o).toFixed(r) : ((e * o - t * o) / o).toFixed(u);
}
function $t(e, t, r) {
  e = e || 0, t = t || 0;
  var n = 0, i = e.toString(), o = t.toString();
  try {
    n += i.split(".")[1].length;
  } catch {
  }
  try {
    n += o.split(".")[1].length;
  } catch {
  }
  return r || r === 0 ? (Number(i.replace(".", "")) * Number(o.replace(".", "")) / Math.pow(10, n)).toFixed(r) : Number(i.replace(".", "")) * Number(o.replace(".", "")) / Math.pow(10, n);
}
function Nt(e, t, r) {
  var n = 0, i = 0, o, u;
  try {
    n = e.toString().split(".")[1].length;
  } catch {
  }
  try {
    i = t.toString().split(".")[1].length;
  } catch {
  }
  return o = Number(e.toString().replace(".", "")), u = Number(t.toString().replace(".", "")), r || r === 0 ? (o / u * Math.pow(10, i - n)).toFixed(r) : o / u * Math.pow(10, i - n);
}
function Bt(e, t) {
  return t = t ? parseInt(t) : 0, t <= 0 ? Math.round(e) : (e = Math.round(e * Math.pow(10, t)) / Math.pow(10, t), e = Number(e).toFixed(t), e);
}
function Pt(e, t = []) {
  return Object.keys(e).sort().reduce(function(r, n) {
    return t.includes(n) || (r[n] = e[n]), r;
  }, {});
}
function W(e, t) {
  let r = e instanceof Object, n = t instanceof Object;
  if (!r || !n)
    return e === t;
  if (Object.keys(e).length !== Object.keys(t).length)
    return !1;
  for (let i in e)
    if (e.hasOwnProperty(i)) {
      let o = e[i] instanceof Object, u = t[i] instanceof Object;
      if (o && u) {
        if (!W(e[i], t[i]))
          return !1;
      } else if (e[i] != t[i])
        return !1;
    }
  return !0;
}
function Lt() {
  let e = (/* @__PURE__ */ new Date()).getTime();
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(r) {
    let n = (e + Math.random() * 16) % 16 | 0;
    return e = Math.floor(e / 16), (r == "x" ? n : n & 3 | 8).toString(16);
  });
}
function Rt(e) {
  e = e || 32;
  let t = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678oOLl9gqVvUuI1", r = t.length, n = "";
  for (let i = 0; i < e; i++)
    n += t.charAt(Math.floor(Math.random() * r));
  return n;
}
function kt(e, t, r) {
  let n = "", i = t, o = [];
  e && (i = Math.round(Math.random() * (r - t)) + t);
  let u = "";
  r = r || "", r == "" && (u = (/* @__PURE__ */ new Date()).getTime());
  let a = "";
  for (let c = 0; c < i; c++)
    a = Math.round(Math.random() * (o.length - 1)), n += o[a];
  return n + u;
}
function qt(e, t) {
  let r = t - e, n = Math.random();
  return e + Math.round(n * r);
}
function Ut(e) {
  let t = /http(s)?:\/\/([^\/]+)/i, r = (e + "").match(t);
  return r != null && r.length > 0 ? r[2] : "";
}
function Qt(e) {
  let t = Z();
  return typeof e > "u" ? t : t[e];
}
function Z() {
  let e = window.location.search || window.location.hash || "", t = [];
  x(e, "?") && (t = U(e, "?").split("&"));
  let r = {};
  for (let n = 0; n < t.length; n++) {
    let i = t[n].split("=");
    i.length === 2 && (r[i[0]] = i[1]);
  }
  return r;
}
function M(e, t) {
  if (t instanceof Array)
    return t.forEach((n) => {
      e = M(e, n);
    }), e;
  let r = e.split("?");
  if (r.length >= 2) {
    let n = encodeURIComponent(t) + "=", i = r[1].split(/[&;]/g);
    for (let o = i.length; o-- > 0; )
      i[o].lastIndexOf(n, 0) !== -1 && i.splice(o, 1);
    return r[0] + (i.length > 0 ? "?" + i.join("&") : "");
  }
  return e;
}
function Ht(e, t) {
  if (typeof t == "object" && Object.prototype.toString.call(t).toLowerCase() == "[object object]" && typeof t.length > "u") {
    e && (e = M(e, Object.keys(t))), e += "", e += e.indexOf("?") === -1 ? "?" : "";
    for (let r in t)
      t.hasOwnProperty(r) && (e += "&" + r + "=" + t[r]);
  }
  return q(e.replace("?&", "?"), "?");
}
function Gt(e) {
  var t = new RegExp("(^|&)" + e + "=([^&]*)(&|$)", "i"), r = window.location.search.slice(1), n = r.match(t);
  if (n != null)
    return decodeURIComponent(n[2]);
  let i = z();
  return i[e] ? i[e] : null;
}
function z(e) {
  e = decodeURIComponent(e);
  let t = {};
  const r = e.split("?")[1];
  if (!r)
    return {};
  const n = r.split("&");
  for (let i in n) {
    const u = n[i].split("=");
    t[u[0]] = u[1];
  }
  return t;
}
function Y(e) {
  return e += "", e = e.replace(/%/g, "%25").replace(/\+/g, "%2B").replace(/ /g, "%20").replace(/\//g, "%2F").replace(/\?/g, "%3F").replace(/&/g, "%26").replace(/\=/g, "%3D").replace(/#/g, "%23"), e;
}
function Wt(e, t = !1, r = !0) {
  t = t || !1;
  let n = t ? "?" : "", i = [];
  for (let o in e) {
    let u = e[o], a = `${o}=${Y(u)}`;
    i.push(a);
  }
  return i.length ? n + i.join("&") : "";
}
function Zt(e, t) {
  var r = t || {};
  for (var n in e) {
    var i = e[n];
    i !== r && (typeof i == "object" ? r[n] = i.constructor === Array ? [] : Object.create(i) : r[n] = i);
  }
  return r;
}
function J(e) {
  let t;
  if (Object.prototype.toString.call(e) == "[object Array]") {
    t = [];
    for (let r in e)
      t.push(J(e[r]));
    return t;
  }
  if (Object.prototype.toString.call(e) == "[object Object]") {
    t = {};
    for (let r in e)
      t[r] = e[r];
    return t;
  }
}
function zt(e) {
  if (typeof e != "object" || e === null) return e;
  if (typeof e.length == "number") {
    let [...t] = e;
    return t;
  } else {
    let { ...t } = e;
    return t;
  }
}
function S(e) {
  if (typeof e != "object" || e === null)
    return e;
  let t;
  if (Array.isArray(e)) {
    t = [];
    for (let r = 0; r < e.length; r++)
      t[r] = S(e[r]);
  } else if (e instanceof Date)
    t = new Date(e.getTime());
  else if (e instanceof RegExp)
    t = new RegExp(e.source, e.flags);
  else {
    t = {};
    for (const r in e)
      Object.prototype.hasOwnProperty.call(e, r) && (t[r] = S(e[r]));
  }
  return t;
}
function Yt(e) {
  var t = parseInt(e), r = 0, n = 0, i = 0;
  t > 60 && (r = parseInt(t / 60), t = parseInt(t % 60), r > 60 && (n = parseInt(r / 60), r = parseInt(r % 60), n > 24 && (i = parseInt(n / 24), n = parseInt(n % 24))));
  var o = "";
  return t > 0 && (o = "" + parseInt(t) + "秒"), r > 0 && (o = "" + parseInt(r) + "分" + o), n > 0 && (o = "" + parseInt(n) + "小时" + o), i > 0 && (o = "" + parseInt(i) + "天" + o), o;
}
let h = {}, p = {};
function Jt(e) {
  return new Promise(async (t, r) => {
    let n = 0;
    for (; p[e] === "loading"; )
      if (await new Promise((o) => setTimeout(o, 1e3)), n++, n > 30)
        return r("加载超时");
    if (p[e] === "loaded")
      return t(!1);
    p[e] = "loading";
    const i = document.createElement("link");
    i.readyState ? i.onreadystatechange = () => {
      (i.readyState == "loaded" || i.readyState == "complete") && (i.onreadystatechange = null, p[e] = "loaded", t(!0));
    } : (i.onload = () => {
      p[e] = "loaded", t(!0);
    }, i.onerror = (o) => {
      p[e] = "error", r(o);
    }), i.rel = "stylesheet", A(e, ".css") ? i.href = e + "?hash=" + window.systemInfo.version : i.href = e, document.getElementsByTagName("head").item(0).appendChild(i);
  });
}
function Kt(e, t) {
  let r = document.createElement("script");
  r.type = "text/javascript", r.readyState ? r.onreadystatechange = () => {
    (r.readyState === "loaded" || r.readyState === "complete") && (r.onreadystatechange = null, t());
  } : r.onload = () => {
    t();
  }, r.src = e, document.body.appendChild(r);
}
function Xt() {
  return !0;
}
function Vt(e) {
  return new Promise(async (t, r) => {
    let n = 0;
    for (; h[e] === "loading"; )
      if (await new Promise((o) => setTimeout(o, 1e3)), n++, n > 30)
        return r("加载超时");
    if (h[e] === "loaded")
      return t(!1);
    h[e] = "loading";
    const i = document.createElement("script");
    i.type = "text/javascript", i.readyState ? i.onreadystatechange = () => {
      (i.readyState === "loaded" || i.readyState === "complete") && (i.onreadystatechange = null, h[e] = "loaded", t(!0));
    } : (i.onload = () => {
      h[e] = "loaded", t(!0);
    }, i.onerror = (o) => {
      h[e] = "error", r(o);
    }), A(e, ".js") ? i.src = e + "?t=" + (/* @__PURE__ */ new Date()).getTime() : i.src = e, document.body.appendChild(i);
  });
}
function er(e, t = 0) {
  return new Promise(async (r, n) => {
    e = $A.originUrl(e);
    let i = 0;
    for (; __load_iframe[e] === "loading"; )
      if (await new Promise((u) => setTimeout(u, 1e3)), i++, i > 30)
        return n("加载超时");
    if (__load_iframe[e] === "loaded")
      return r(!1);
    __load_iframe[e] = "loading";
    const o = document.createElement("iframe");
    o.style.display = "none", o.src = e, o.onload = () => {
      __load_iframe[e] = "loaded", r(!0), t > 0 && setTimeout(() => {
        document.body.removeChild(o), delete __load_iframe[e];
      }, t);
    }, o.onerror = (u) => {
      __load_iframe[e] = "error", n(u);
    }, document.body.appendChild(o);
  });
}
const K = function(e, t) {
  const r = {
    "y+": e.getFullYear().toString(),
    // 年
    "M+": (e.getMonth() + 1).toString(),
    // 月份
    "d+": e.getDate().toString(),
    // 日
    "h+": e.getHours().toString(),
    // 小时
    "m+": e.getMinutes().toString(),
    // 分
    "s+": e.getSeconds().toString(),
    // 秒
    "q+": Math.floor((e.getMonth() + 3) / 3),
    // 季度
    S: e.getMilliseconds(),
    // 毫秒
    "w+": e.getDay(),
    // 星期
    "a+": e.getDay(),
    // 星期
    W: e.getDay(),
    // 星期
    GMT: e
  };
  for (const n in r) {
    n == "W" && (r[n] == 0 && (r[n] = "日"), r[n] == 1 && (r[n] = "一"), r[n] == 2 && (r[n] = "二"), r[n] == 3 && (r[n] = "三"), r[n] == 4 && (r[n] = "四"), r[n] == 5 && (r[n] = "五"), r[n] == 6 && (r[n] = "六"));
    const i = new RegExp("(" + n + ")").exec(t);
    i && (t = t.replace(i[1], i[1].length === 1 ? r[n] : r[n].padStart(i[1].length, "0")));
  }
  return t;
};
class X {
  constructor() {
    let t = /* @__PURE__ */ new Date();
    return this.dateObj = t, this._formatType = "yyyy-MM-dd hh:mm:ss", this;
  }
  setYear(t = 0) {
    return t == 0 || t == "" || this.dateObj.setFullYear(this.dateObj.getFullYear() + t), this;
  }
  setMonth(t = 0) {
    return t == 0 || t == "" || this.dateObj.setMonth(this.dateObj.getMonth() + t), this;
  }
  setDay(t = 0) {
    return t == 0 || t == "" || this.dateObj.setDate(this.dateObj.getDate() + t), this;
  }
  /**
   * 设置时间戳
   */
  setTime(t) {
    return t = t || "", t != "" && (t.toString().length == 10 && (t = parseInt(t) * 1e3), this.dateObj.setTime(t)), this;
  }
  init(t) {
    return Object.prototype.toString.call(t) == "[object Number]" ? this.setTime(t) : (Object.prototype.toString.call(t) == "[object Date]" || Object.prototype.toString.call(t) == "[object String]") && (this.dateObj = new Date(t)), this;
  }
  format(t = "yyyy-MM-dd hh:mm:ss") {
    return t = t || "", t != "" && (this._formatType = t), this;
  }
  /** 返回结果为 string */
  _valToString() {
    return K(this.dateObj, this._formatType);
  }
  /** 返回结果为 int */
  _valToInt() {
    let t = this.dateObj.getTime();
    return t = parseInt(t) / 1e3, t;
  }
  /** 返回结果为 object */
  _valToObject() {
    let t = {
      year: this.dateObj.getFullYear(),
      //年
      month: this.dateObj.getMonth() + 1,
      //月
      day: this.dateObj.getDate(),
      //日
      hour: this.dateObj.getHours(),
      //时
      minute: this.dateObj.getMinutes(),
      //分
      second: this.dateObj.getSeconds(),
      //秒
      millisecond: this.dateObj.getMilliseconds(),
      //秒
      quarter: Math.floor((this.dateObj.getMonth() + 3) / 3),
      // 季度
      week: this.dateObj.getDay(),
      // 星期
      week_cn: this.dateObj.getDay()
      // 中文星期
    };
    for (let r in t) {
      let n = t[r];
      r != "y" && (r === "a" || r == "week" ? t[r] = n : r === "week_cn" ? (n == 0 && (t[r] = "日"), n == 1 && (t[r] = "一"), n == 2 && (t[r] = "二"), n == 3 && (t[r] = "三"), n == 4 && (t[r] = "四"), n == 5 && (t[r] = "五"), n == 6 && (t[r] = "六")) : t[r] = String(n < 10 ? "0" + n : n));
    }
    return t;
  }
  _valToArray() {
  }
  value(t) {
    t = t || "string";
    let r;
    return t == "string" ? r = this._valToString() : t == "object" ? r = this._valToObject() : t == "int" && (r = this._valToInt()), r;
  }
}
function tr() {
  return new X();
}
function rr(e) {
  let t = 0;
  const r = (n) => {
    t = n, e.innerHTML = `count is ${t}`;
  };
  e.addEventListener("click", () => r(++t)), r(0);
}
export {
  tr as LibsDate,
  X as LibsDateClass,
  Mt as amountSplit3,
  St as amountToChinese,
  V as arrayConcat,
  te as arrayExtend,
  ae as arrayFilterColumns,
  se as arrayInArray,
  oe as arrayInText,
  ue as arrayInValue,
  re as arrayLast,
  fe as arrayLength,
  ne as arrayLetterSort,
  ee as arrayMixin,
  ie as arrayParseType,
  j as arrayToTree,
  ce as arrayUnique,
  jt as baseDebounce,
  Ot as baseThrottle,
  zt as cloneData,
  S as cloneDeep,
  vt as colorRandomRgba,
  H as colorRgbaToHexify,
  Tt as colorToImgRbg,
  It as fileBytesToSize,
  Dt as fileCheckImg,
  G as fileGetExt,
  _t as fileImgCompress,
  Ct as geoCalcDistance,
  Be as hasDevice_internet,
  me as isBank,
  de as isBrowserType,
  C as isBrowser_chrome,
  I as isBrowser_edge,
  F as isBrowser_firefox,
  he as isBrowser_ie,
  pe as isBrowser_ie11,
  E as isBrowser_opera,
  be as isBrowser_qq,
  $ as isBrowser_safari,
  ge as isBrowser_webkit,
  ye as isBrowser_weibo,
  N as isBrowser_weixin,
  Pe as isDecimal,
  Ne as isDevice_android,
  Ie as isDevice_desktop,
  Ee as isDevice_ios,
  $e as isDevice_ipad,
  Fe as isDevice_iphone,
  Ce as isDevice_mobile,
  Ae as isEmail,
  je as isEmoji,
  ve as isEmpty,
  _e as isEmptyObject,
  De as isEmptyTrim,
  m as isHave,
  ut as isIdCard,
  Se as isLandline,
  we as isMobile,
  Le as isNumCode,
  ke as isNumInt,
  Re as isNumNumber,
  qe as isNumPosi,
  Ue as isNumZS,
  xe as isPostal,
  Te as isRequire,
  Ze as isStrAbroad,
  We as isStrCHS,
  Qe as isStrEN,
  ze as isStrEnNum,
  He as isStrZh,
  Ge as isStrZhOrEn,
  Ye as isTypeArray,
  Je as isTypeBoolean,
  Ke as isTypeDate,
  Xe as isTypeFunction,
  st as isTypeJson,
  ot as isTypeJsonString,
  Ve as isTypeNull,
  et as isTypeNumber,
  tt as isTypeObj,
  it as isTypeRegExp,
  rt as isTypeString,
  nt as isTypeUndefined,
  Oe as isUrl,
  Me as isUrlHttp,
  Jt as loadCssPromise,
  er as loadIframePromise,
  Kt as loadScript,
  Vt as loadScriptPromise,
  Xt as loadScriptSync,
  Et as numberAccAdd,
  Nt as numberAccDiv,
  $t as numberAccMul,
  Ft as numberAccSub,
  Bt as numberRound,
  Zt as objDeepClone,
  J as objDeepCopy,
  W as objectEquals,
  Pt as objectSort,
  Q as parseNumber,
  qt as randomNum,
  Rt as randomString,
  Lt as randomUUID,
  kt as randomWord,
  Yt as secondsFormat,
  rr as setupCounter,
  x as strExists,
  lt as strFeed,
  At as strFilterCN,
  gt as strFormatMoney,
  bt as strFourSeparate,
  U as strGetMiddle,
  yt as strHideBankNo,
  mt as strHideMobile,
  dt as strLeftDelete,
  k as strLeftExists,
  ft as strLength,
  pt as strReplace,
  q as strRightDelete,
  A as strRightExists,
  xt as strSplitChars,
  wt as strSubString,
  ct as strTrimAll,
  at as strTrimSide,
  ht as str_ends_with,
  le as treeCalcLevel,
  D as treeDeepInclude,
  _ as treeRegDeepParents,
  v as treeToSubFlatten,
  T as treeToSupFlatten,
  Gt as uriGetParam,
  Ht as urlAddParams,
  Ut as urlGetDomain,
  Z as urlGetParameterAll,
  Wt as urlObjToQuery,
  Qt as urlParameter,
  z as urlQueryToObj,
  M as urlRemoveParameter
};
