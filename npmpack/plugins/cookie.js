class CookieClass {
  getData(key) {}
  setData(key, value) {}
}
export default CookieClass;
/*
 * 设置cookie
 */
function setCookie(name, value, expiredays, path) {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + expiredays);
  exdate.setMinutes(exdate.getMinutes() + expiredays);
  document.cookie = name + '=' + escape(value) + (expiredays == null ? '' : ';expires=' + exdate.toGMTString() + ';path=/');
  document.cookie = name + '=' + escape(value) + (expiredays == null ? '' : ';expires=' + exdate.toGMTString()) + ';path=/';
}
function setCookie(val) {
  //cookie设置[{key:value}]、获取key、清除['key1','key2']
  for (var i = 0, len = val.length; i < len; i++) {
    for (var key in val[i]) {
      document.cookie = key + '=' + encodeURIComponent(val[i][key]) + '; path=/';
    }
  }
}
function setCookie(name, value, options) {
  const myWindow = window;
  let cookieStr = myWindow.escape(name) + '=' + myWindow.escape(value) + ';';
  if (!options) {
    options = {};
  }
  if (options.expires) {
    const dtExpires = new Date(new Date().getTime() + options.expires * 1000 * 60 * 60 * 24);
    cookieStr += 'expires=' + dtExpires.toUTCString() + ';';
  }
  if (options.path) {
    cookieStr += 'path=' + options.path + ';';
  }
  if (options.domain) {
    cookieStr += 'domain=' + options.domain + ';';
  }
  document.cookie = cookieStr;
}
/*
 * 获取cookie
 */
export const getCookie = (name) => {
  var arr,
    reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)');
  if ((arr = document.cookie.match(reg))) {
    return unescape(arr[2]);
  } else {
    return null;
  }
};

function getCookie(name) {
  let arr = [];
  const reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)');
  if ((arr = document.cookie.match(reg))) {
    return decodeURIComponent(arr[2]);
  } else {
    return '';
  }
}
function getCookie(c_name) {
  if (document.cookie.length > 0) {
    c_start = document.cookie.indexOf(c_name + '=');
    if (c_start != -1) {
      c_start = c_start + c_name.length + 1;
      c_end = document.cookie.indexOf(';', c_start);
      if (c_end == -1) c_end = document.cookie.length;
      return unescape(document.cookie.substring(c_start, c_end));
    }
  }
  return '';
}
function getCookie(name) {
  var strCookie = document.cookie;
  var arrCookie = strCookie.split('; ');
  for (var i = 0, len = arrCookie.length; i < len; i++) {
    var arr = arrCookie[i].split('=');
    if (name == arr[0]) {
      return decodeURIComponent(arr[1]);
    }
  }
}

/*
 * 删除cookie
 */
function delCookie(name) {
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval = getCookie(name);
  if (cval != null) document.cookie = name + '=' + cval + ';expires=' + exp.toGMTString() + ';path=/';
}
function delCookie(name, options) {
  if (this.getCookie(name)) {
    if (!options) {
      options = {};
    }
    options.expires = -1;
    this.setCookie(name, '', options);
  }
}
function checkCookie() {
  var cookiename = getCookie('canshow');
  if (cookiename != null && cookiename != '') {
    canshow = !1;
  } else {
    $('#tuiJb')
      .find('.tuiB_c')
      .bind('click', function () {
        canshow = !1;
        cookiename = 'canshow';
        if (cookiename != null && cookiename != '') {
          setCookie('canshow', cookiename, 30, '/');
        }
      });
  }
}
function clearCookie(name) {
  var myDate = new Date();
  myDate.setTime(-1000); //设置时间
  for (var i = 0, len = name.length; i < len; i++) {
    document.cookie = '' + name[i] + "=''; path=/; expires=" + myDate.toGMTString();
  }
}
