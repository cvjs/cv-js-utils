const fs = require('fs');

function generateFileMd(titleName, fileContent, docsPath) {
  /**
   * 解析文件
   */
  // 正则表达式来匹配注释块中的信息，包括 @action 标签
  // const regex = /\/\*\*([\s\S]*?)\*\/[\s\S]*?function\s+(\w+)\s*\(([\s\S]*?)\)/g;
  // const regex = /\/\*\*([\s\S]*?)\*\/\s*function\s+(\w+)\s*\(([\s\S]*?)\)/g;
  // const regex = /\/\*\*\s*([^]*?)\s*\*\/\s*function\s+(\w+)\s*\(([^]*?)\)/g;
  const regex = /\/\*\*\s*([^]*?)\s*\*\/\s*function\s+(\w+)\s*\(([^]*?)\)/g;

  let mdTbNameLenMax = 0;
  let mdTbActionLenMax = 0;
  let docsArr = [];
  let match;
  while ((match = regex.exec(fileContent)) !== null) {
    // const comment = match[1].trim();
    // 捕获组1匹配注释，如果没有注释则为空字符串
    const comment = match[1] ? match[1].trim() : '';
    const functionName = match[2];
    const functionLink = functionName.toLocaleLowerCase();
    const params = match[3].split(',').map((param) => param.trim());

    // 提取 @action 标签中的动作名称
    // const actionMatch = /\/\*\*\s*\*\s*@action\s*(.*?)\s*\*\//.exec(comment);
    // const actionMatch = /@action\s+(.*?)\s*(?:\*\/|$)/.exec(comment);
    const actionMatch = comment.match(/@action\s+(.*)/);
    // const actionMatch = comment ? /@action\s+(.*?)\s*(?:\*\/|$)/.exec(comment) : null; // 如果有注释，则匹配 @action 标签
    const actionName = actionMatch ? actionMatch[1].trim() : null;
    /**
     * 返回类型
     */
    // const returnMatch = comment.match(/@return\s+(.*)/);
    const returnMatch = comment.match(/@returns\s*\{([^}]*)\}\s*(.*)/);
    let returnName = returnMatch ? returnMatch[1].trim() : '-';
    if (returnName) {
      returnName = returnName.replace('{', '');
      returnName = returnName.replace('}', '');
    }
    if (actionName) {
      console.log(`
Function：${functionName}   
Action：${actionName}   
Return：${returnName}
`);
      // console.log('Comment:', comment);
      console.log('Params:', params);
      // console.log('------------------------');

      let currNameLeng = `[${functionName}](#${functionLink})`.length;
      if (currNameLeng > mdTbNameLenMax) {
        mdTbNameLenMax = currNameLeng;
      }
      let currActionLeng = `${actionName}`.length;
      if (currActionLeng > mdTbActionLenMax) {
        mdTbActionLenMax = currActionLeng;
      }
      docsArr.push({
        function: functionName,
        functionLink: functionLink,
        action: actionName,
        params: params,
        return: returnName
      });
    }
  }
  console.log(mdTbNameLenMax);

  let tableHeadName = '方法'.padEnd(mdTbNameLenMax - 1);
  let tableHeadFenge = `:--------------------------`.padEnd(mdTbNameLenMax, '-');
  let mdContent = `# ${titleName}

### 方法概览

| ${tableHeadName}| 返回   | 说明                     |
| ${tableHeadFenge} | :----- | :----------------------- |
`;
  for (let i in docsArr) {
    let docsItem = docsArr[i];
    let nameStr = `[${docsItem['function']}](#${docsItem['functionLink']})`;
    nameStr = nameStr.padEnd(mdTbNameLenMax + 1);
    let actionStr = `${docsItem['action']}`;
    actionStr = actionStr.padEnd(mdTbActionLenMax + 1);
    mdContent += `| ${nameStr}| ${docsItem['return']} | ${actionStr}|` + '\n';
  }

  fs.writeFileSync(docsPath, mdContent, 'utf8');
}
function showConsoleLog() {
  console.log(...arguments);
}
// generateMd

const parseDocsDirList = ['function'];
for (let i_dir in parseDocsDirList) {
  // 目录名称
  let dirName = parseDocsDirList[i_dir];

  // md菜单
  let mdDirMenuPath = `../../docs/${dirName}/_menu.js`;
  let mdDirMenuContet = 'export default [\n';

  // 指定要读取的目录
  let directoryPath = `../../src/${dirName}`;
  // 读取目录
  const dirFileList = fs.readdirSync(directoryPath);
  const dirFileLeng = dirFileList.length;
  for (let i_file in dirFileList) {
    let fileName = dirFileList[i_file];
    let filePath = `${directoryPath}/${fileName}`;
    let docsName = fileName.replace('.js', '.md');
    let docsPath = `../../docs/${dirName}/${docsName}`;
    /**
     * 读取内容
     */
    const fileContent = fs.readFileSync(filePath, 'utf8');
    /**
     * 获取【标题】
     */
    const titleRegex = fileContent.match(/@title\s+(.*)/);
    const titleName = titleRegex ? titleRegex[1].trim() : null;
    showConsoleLog(`文件名称：${fileName} ；标题：${titleName}`);
    /**生成文档 */
    generateFileMd(titleName, fileContent, docsPath);

    let mdFileName = fileName.replace('.js', '');
    let mdFileLink = `/function/${mdFileName}`;

    if (dirFileLeng == ++i_file) {
      mdDirMenuContet += `  { text: '${titleName}', link: '${mdFileLink}' }\n`;
    } else {
      mdDirMenuContet += `  { text: '${titleName}', link: '${mdFileLink}' },\n`;
    }
  }
  mdDirMenuContet += '];\n';
  fs.writeFileSync(mdDirMenuPath, mdDirMenuContet, 'utf8');
}

// ~~~
// geoCalcDistance(to_lat, to_lng, my_lat, my_lng)
// ~~~

// **参数**

// | 参数   | 默认值        | 说明 |
// | :----- | :------------ | :--- |
// | to_lat | 对象 纬度     | 必填 |
// | to_lng | 对象 经度     | 必填 |
// | my_lat | 当前我的 纬度 | 必填 |
// | my_lng | 当前我的 经度 | 必填 |
