# StorageClass 

### 简要

>数据存储

**使用说明**

添加 `src/plugins/storage.js` 扩展

```js
import StorageClass from '@10yun/cv-js-utils/plugins/storage.js';

const StorageObj = new StorageClass({
  prefix: 'sy_'
});

export default StorageObj;
```

在调用的地方

```js
import StorageObj from '@/plugins/storage';

StorageObj.localSet();
StorageObj.localGet();

```





## 离线数据库操作

-  localforage.min.js
http://localforage.docschina.org
