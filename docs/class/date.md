# LibsDate

**简介**

- 类LibsDateClass
- 链式调用

**出口**

- 类LibsDateClass


### 方法

| 方法      | 说明                       |
| :-------- | :------------------------- |
| init      | 初始值，time 或者 date     |
| format    | 设置格式                   |
| value     | 返回值，默认字符串（日期） |
| setYear   | 设置年                     |
| setMonth  | 设置月                     |
| setDay    | 设置日                     |
| setHour   | 设置时                     |
| setMinute | 设置分                     |
| setTime   | 设置时间戳，会覆盖init     |

### 快速使用

- 引入 

```js

// cdn 全局引入

// 调用全局单个
import { LibsDate } from "@10yun/cv-js-utils";


// 调用局部（已经实例过）
import LibsDate from "@10yun/cv-js-utils/libs/LibsDate";


// 调用局部（自己实例）
import LibsDateClass from "@10yun/cv-js-utils/libs/LibsDate";
var LibsDate = new LibsDateClass();

```


### 调用示例


```js

// 当前 年月日时分秒
LibsDate().value();

// 当前 年月日
LibsDate().format('yyyy-MM-dd').value();
// 当前 年月
LibsDate().format('yyyy-MM').value();
// 昨天 年月日
LibsDate().setDay(-1).format('yyyy-MM-dd').value();
// 往后 5天
LibsDate().setDay(+5).format('yyyy-MM-dd').value();
// 上个月又上一天
LibsDate().setDay(-1).setMonth(-1).format('yyyy-MM-dd').value();

// 时间戳转日期，默认格式
LibsDate().init(1622974980).value();
// 时间戳转日期，自定义格式
LibsDate().init(1622974980).format('yyyy-MM-dd').value();

// 日期转时间戳
LibsDate().init('2021-06-06').value('int');
LibsDate().init('2021-06-06 18:23:00').value('int')

// value转对象形式，只支持返回日期数组
LibsDate()...value('object');

```

### 链式.init(timeOrDate)


**参数**

| 参数       | 类型        | 默认 | 可选 |
| :--------- | :---------- | :--- | :--- |
| timeOrDate | string、int | -    | -    |


### 链式.format(formatStr)

**参数**

| 参数      | 类型        | 默认 | 可选           |
| :-------- | :---------- | :--- | :------------- |
| formatStr | string、int | -    | 详见可选值列表 |

- formatStr 可选值  
格式中的分割符`-`,`:` 可改为自己想要的分割符号`/`、` `、等其他   
可任意组合，如下

| 格式                  | 返回                                             | 说明         |
| :-------------------- | :----------------------------------------------- | :----------- |
| null                  | 2020-01-02 03:04:05                              | 为空不填     |
| yyyy-MM-dd hh:mm:ss   | 2020-01-02 03:04:05                              | 默认         |
| yyyy-MM-dd hh:mm      | 2020-01-02 03:04                                 |              |
| yyyy-MM-dd            | 2020-01-02                                       |              |
| yyyy-03-15            | 2020-03-15                                       | 直接指定几日 |
| yyyy/MM/dd            | 2020/01/02                                       |              |
| yyyy                  | 2020                                             |              |
| hh:MM                 | 03:04                                            |              |
| hh:MM:ss              | 03:04:05                                         |              |
| yyyy-MM-dd 星期 w     | 2020-01-01 星期 1                                |              |
| yyyy年MM月dd日 星期 w | 2020年01月01日 星期 1                            | 星期，小写w  |
| yyyy年MM月dd日 星期 W | 2020年01月01日 星期 一                           | 星期，大写W  |
| yyyy-MM-dd 季度 q     | 2020-01-01 季度 1                                |              |
| GMT                   | Thu Jan 02 2020 03:04:05 GMT+0800 (中国标准时间) |              |


- 格式说明

假如当前时间为 `2020-01-02 03:04:05`

| 字段格式 | 返回 | 说明 |
| :------- | :--- | :--- |
| yyyy     | 2020 | 年   |
| MM       | 02   | 月   |
| dd       | 02   | 日   |
| hh       | 02   | 时   |
| mm       | 01   | 月   |
| ss       | 02   | 秒   |
| w        | 02   | 星期 |

### 链式.value(type)
--- 

**参数**

| 参数 | 类型   | 默认     | 可选              |
| :--- | :----- | :------- | :---------------- |
| type | string | "string" | string/object/int |
 
**返回**

- 当 value() 或者 value('string') 时 
 
```js
// 日期
2021-06-06 18:42:11
```
- 当 value('int') 时 
```js
// 时间戳
1622974980
```
- 当 value('object') 时

```js
{
    day       : "04",   // 日
    hour      : "20",   // 时
    ms        : "208",  // 毫秒
    minute    : "09",   // 分
    month     : "03",   // 月
    quarter   : "01",   // 季度
    second    : "27",   // 秒
    week      : "06",   // 星期
    week_cn    : "六",   // 星期，中文
    year      : "2023", // 年
}
```



### 参考

有些语法过期

http://www.icodebang.com/article/325053