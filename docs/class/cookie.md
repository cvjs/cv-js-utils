# LibsCookie

**简介**

- 类 LibsCookieClass
- 链式调用


```js

import CookieClass from "@10yun/cv-js-utils";

Cookie.set('name', 'value', { expires: 365, path: '/' });
Cookie.get('name'); // => 'value'
Cookie.remove('name');

```