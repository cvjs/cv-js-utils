export default [
  { text: '介绍', link: '/class/' },
  {
    text: '网络请求',
    collapsed: false,
    items: [
      { text: '介绍', link: '/class/request/' },
      { text: '配置', link: '/class/request/sett' },
      { text: 'flag 请求（推荐）', link: '/class/request/flag' },
      { text: 'url 请求', link: '/class/request/url' },
      { text: '其他方法', link: '/class/request/function' }
    ]
  },
  {
    text: '缓存',
    collapsed: false,
    items: [{ text: '介绍', link: '/class/storage/' }]
  },
  { text: '日期', link: '/class/date' },
  { text: 'Cookie', link: '/class/cookie' },
  {
    text: '加密',
    collapsed: false,
    items: [
      { text: '安全加密 Base64', link: '/class/security_base64' },
      { text: '安全加密 MD5', link: '/class/security_md5' },
      { text: '安全加密 AES', link: '/class/security_aes' },
      { text: '安全加密 RSA', link: '/class/security_rsa' }
    ]
  }
];
