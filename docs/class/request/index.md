# RequestClass 实例

### 简要

- 数据请求、网络请求
- 支持 vue3 项目，基于 axios 封装
- 支持 uniapp 项目，基于 uni.request 封装
- 支持 nuxt3 服务端渲染，基于 useFetch 封装

### 构造参数

| 参数        | 类型     | 说明                                       | 示例 |
| :---------- | :------- | :----------------------------------------- | :--- |
| baseURL     | String   | 接口请求的基础url路径                      |      |
| flagFunc    | Function | 重新获取flag的方法                         |      |
| flagMap     | Object   | flag方式请求需配置                         |      |
| needMethods | Array    | 需要转换的methods，支持 PUT、PATCH、DELETE |      |
| headers     | Object   | 每次请求携带的 header 变量                 |      |
| requests    | Object   | 每次请求携带的 request 变量                |      |


### 方法

- **设置参数**

| 方式       | 说明        | 示例 |
| :--------- | :---------- | :--- |
| setFlag    | 设置flag    |      |
| setHeader  | 设置header  |      |
| setRequest | 设置request |      |


- **flag 请求（推荐）**

| 方式       | 说明                              | 示例                                  |
| :--------- | :-------------------------------- | :------------------------------------ |
| flagGet    | GET请求,获取多条、一条            | [查看](/class/request/flag#flagget)   |
| flagPost   | POST请求，提交、新增、更新 、删除 | [查看](/class/request/flag#flagpost)  |
| flagPut    | PUT请求，修改全部字段，更新数据   | [查看](/class/request/flag#flagput)   |
| flagPatch  | PATCH请求，修改单个字段，更新部分 | [查看](/class/request/flag#flagpatch) |
| flagDel    | DELETE请求，删除数据              | [查看](/class/request/flag#flagdel)   |
| flagUpload | POST请求，上传                    |                                       |


- **url 请求**

| 方式      | 说明           | 示例                                   |
| :-------- | :------------- | :------------------------------------- |
| urlGet    | GET请求数据    | [查看](/class/request/url.html#urlget) |
| urlPost   | POST提交数据   | [查看](/class/request/url)             |
| urlPut    | PUT更新数据    |                                        |
| urlPatch  | PATCH更新部分  |                                        |
| urlDel    | DELETE删除数据 |                                        |
| urlUpload | POST请求，上传 |                                        |



### 重写功能

| 方式             | 说明           | 示例 |
| :--------------- | :------------- | :--- |
| resetNetError    | 重写错误展示   |      |
| resetNetResponse | 重写响应拦截器 |      |


