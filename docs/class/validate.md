# 表单 验证

| 方法              | 返回    | 说明                 |
| :---------------- | :------ | :------------------- |
| formRules         | Boolean | 表单验证             |
| formCheckValidate | Boolean | 当场景为空的时候验证 |

### formRules(rules, messages, data) 
> 表单验证

| 传值     | 类型   | 说明         | 示例                                        |
| :------- | :----- | :----------- | :------------------------------------------ |
| rules    | Object | 要验证的内容 | { "username":{"require":true} }             |
| messages | Object | 要提示的内容 | { "username":{"require":"请上传特权主图"} } |
| data     | Object | Object       |                                             |

返回值

```json
{
    status: 200,
    data: {},
    msg: '验证成功'
}
//或
{
    status: 404,
    data: {
        key,
        method: key1
    },
    msg: messages[key][key1]
}
```

### formCheckValidate(rules, messages, scene, data)
> 表单验证

| 传值     | 类型   | 说明         | 示例                                        |
| :------- | :----- | :----------- | :------------------------------------------ |
| rules    | Object | 要验证的内容 | { "username":{"require":true} }             |
| messages | Object | 要提示的内容 | { "username":{"require":"请上传特权主图"} } |
| scene    | Object |              |                                             |
| data     | Object | Object       |                                             |

返回值

```json
{
    status: true,
    data: {},
    msg: '验证成功'
}
//或
{
    status: false,
    data: {
        key,
        method: key1
    },
    msg: messages[key][key1]
}
```

## 表单提交验证器


在 _serve 下 任意地方，文件命名 ( [xxx]_validate.js ),xxx为任意名，框架将自动加载
```js
import { LibsValidate } from "@10yun/cv-js-utils";

let checkResult = ApplyValidate.setScene([
	'apply_type',
	'apply_username',
	'apply_usermobile',
]).checkScene(this.applyData);
if (checkResult.success != true) {
  console.log(checkResult.msg);
}
  
var XxxValidate = {
	rules: {
		'xxxa': ['require', 'isRealName'],
		xxxb: ['require', 'isMobile',],
		xxxc: ['require', 'isIdCard'],
	},
	messages: {
		xxxa: {
			require: '姓名不能为空',
			isRealName: '请输入1-6位的汉字、英文、数字、下划线'
		},
		xxxb: {
			require: '电话不能为空',
			isMobile: '您输入手机格式有误',
		},
		xxxc: {
			require: '电话不能为空',
			isIdCard: '您输入身份证格式有误'
		},
	},
	scene: [],
	//设置场景
	setScene: function (setFData) {
		this.scene = setFData;
		return this;
	},
	//验证场景
	checkScene: function (formData) {
		return HelperValidate.formCheckValidate(this.rules, this.messages, this.scene, formData)
	}
}

export default XxxValidate;


```
