## 安全加密 AES加密

依赖其他
```
cnpm install crypto-js --save-dev
cnpm install jsencrypt --save-dev
cnpm install js-md5 --save-dev
```


 ## AES加密 方法

| 方法          | 返回   | 说明    | 参数               |
| :------------ | :----- | :------ | :----------------- |
| secAesEncrypt | String | aes加密 | (data,code,key,iv) |
| secAesDecrypt | String | aes解密 | (data,code,key,iv) |

```
//AES-128-CBC加密模式，key需要为16位，key和iv可以一样
## 加密
ccUtils.secAesEncrypt('hello world', 'base64','123456', 'p9w6c50cyox1ka13');
## 解密
ccUtils.secAesDecrypt("NzVBM0VCRkVBMTczNDlDMUIxNUVBMDJCRTRCNDNBNzVGNjQ3MzEzQTE1MTUxMTEyNjNCNDhCQjQ0MDA1MkY5QQ==", 'hex');
```

