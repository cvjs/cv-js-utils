 ## 安全加密 RSA加密

依赖其他
```
cnpm install crypto-js --save-dev
cnpm install jsencrypt --save-dev
cnpm install js-md5 --save-dev
```

 ## RSA加密 方法

| 方法          | 返回   | 说明    | 参数        |
| :------------ | :----- | :------ | :---------- |
| secRsaEncrypt | String | rsa加密 | (data,code) |
| secRsaDecrypt | String | rsa解密 | (data,code) |

```
var publickey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDKrL8/V5GLN7ix9Fk2aHNCe54rvD58dbD1kRGfpVuYBh6iuZd+3+WwJUPnoTIvnayYdHdz7BLXmDT4w9XwDCwhb2/vRXnYvl1+i1lQrsLQYXjFfqdFrlVzaFjTZvuhyPSPrfHJntuPCTUmr1yVGjfTc5mjxQFfSyzQuydlul+gAQIDAQAB";
var privatekey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMqsvz9XkYs3uLH0WTZoc0J7niu8Pnx1sPWREZ+lW5gGHqK5l37f5bAlQ+ehMi+drJh0d3PsEteYNPjD1fAMLCFvb+9Fedi+XX6LWVCuwtBheMV+p0WuVXNoWNNm+6HI9I+t8cme248JNSavXJUaN9NzmaPFAV9LLNC7J2W6X6ABAgMBAAECgYBY3rCplLtUIWXSWkO4MMrBjzUHFm8L4gtrUmY466gjHCPY1KqSTpkHyP9ilPNNn8TrwL4UTaLheHNZ1X182cmLQ/AWHo+bIZHkiM06JMJxR3NCHAKTq4k7xDAPC4r+W/IZ0PtuKKlhRPQPnlfFRz4KNp44EH6zAl2GgvvcDuY3VQJBAOP/N173VOy/b1wdBf5k2PTpBJXvZ7AkVx/P8tKZ/q/IVM/BhP2YkqwD+HM5KG9Lr1uRAD8GXj19aEEkAcsmKcsCQQDjkVcfezjBWbssHqUpmtZLdk5l/ZiKz7ivFubD/Mb/546x665l9CyL5VIYsEBgBuE/nBxgfwNJ9FnjmdWre5PjAkEA1Vjm6XisAXYJsLp/91NLKbI61krWTh9TX7NpV+U+TCM8KcS+u8dfyJNWlAuyaKL8DsUa498DpKqLOhmFjCHEDQJAXa1csebljCJF/Hl4/9FvWY0P5Mgp3Sp5GgpRYGhT+s/1W6RrHCkRlV0HGIalWWI7oxW+ULK4Rd9CiYCwBnFOhQJBAKgoXL2V9RcUldQHVQF11ndAoY3+j71iBeOmdY9qy0WuoDWsmjvIRWb6VE94CiYwHcelkt/aUrQ1vnGu2SqbPRo=";

var encrypt = ccUtils.secRsaEncrypt('helloworld', 'hex',publickey, privatekey);
console.log(encrypt);
console.log(rsa.secRsaDecrypt(encrypt, 'hex',publickey, privatekey));
```

