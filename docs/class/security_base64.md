## 安全加密 base64

依赖其他
```
cnpm install crypto-js --save-dev
cnpm install jsencrypt --save-dev
cnpm install js-md5 --save-dev
```

## base64 方法 

| 方法         | 返回   | 说明    | 参数     |
| :----------- | :----- | :------ | :------- |
| secMd5Encode | String | md5加密 | (String) |


1.Base64编码
```javascript
Utils.Base64.encode("12");
```

2.Base64解码
```javascript
Utils.Base64.decode("MTI=")
```

