# array基础

### 方法概览

| 方法                                      | 返回   | 说明                     |
| :---------------------------------------- | :----- | :----------------------- |
| [arrayConcat](#arrayconcat)               | Array | 合并多个数组          |
| [arrayMerge](#arraymerge)                 | Array | 数组合并            |
| [arrayMixin](#arraymixin)                 | Array | 数据对象拓展          |
| [arrayLast](#arraylast)                   | Boolean,String | 获取数组最后一个值       |
| [arrayLetterSort](#arraylettersort)       | Array | 数组根据英文字母排序      |
| [arrayParseType](#arrayparsetype)         | Array | 数组解析，格式转换       |
| [arrayInText](#arrayintext)               | Boolean | 判断是否包含在数组里      |
| [arrayInValue](#arrayinvalue)             | Array | 数组是否包含某值        |
| [arrayFilterColumns](#arrayfiltercolumns) | Array | 过滤数组，需要的字段      |
| [arrayUnique](#arrayunique)               | Array | 一维数组去重(字符串/数字)  |
| [arrayLength](#arraylength)               | Number | 获取数组长度（处理数组不存在） |
