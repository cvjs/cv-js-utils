# 检测浏览器

### 方法概览

| 方法                                    | 返回   | 说明                     |
| :-------------------------------------- | :----- | :----------------------- |
| [isBrowserType](#isbrowsertype)         | String | 返回浏览器类型         |
| [isBrowser_ie](#isbrowser_ie)           | Boolean | 是否 IE           |
| [isBrowser_ie11](#isbrowser_ie11)       | Boolean | 是否 IE11         |
| [isBrowser_edge](#isbrowser_edge)       | Boolean | 是否 Edge浏览器      |
| [isBrowser_chrome](#isbrowser_chrome)   | Boolean | 是否chrome浏览器     |
| [isBrowser_opera](#isbrowser_opera)     | Boolean | 是否opera浏览器      |
| [isBrowser_firefox](#isbrowser_firefox) | Boolean | 是否Firefox浏览器    |
| [isBrowser_safari](#isbrowser_safari)   | Boolean | 是否Safari浏览器     |
| [isBrowser_webkit](#isbrowser_webkit)   | Boolean | 是否苹果、chrome谷歌内核 |
| [isBrowser_weixin](#isbrowser_weixin)   | Boolean | 是否是微信           |
| [isBrowser_qq](#isbrowser_qq)           | Boolean | 是否qq            |
| [isBrowser_weibo](#isbrowser_weibo)     | Boolean | 是否微博            |
