# 数字金额

### 方法概览

| 方法                                | 返回   | 说明                     |
| :---------------------------------- | :----- | :----------------------- |
| [amountToChinese](#amounttochinese) | String | 数字金额转中文       |
| [amountSplit3](#amountsplit3)       | Number | 将数字\金额转成3位分隔符 |
