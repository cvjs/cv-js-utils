# 检测数字

### 方法概览

| 方法                        | 返回   | 说明                     |
| :-------------------------- | :----- | :----------------------- |
| [isDecimal](#isdecimal)     | Boolean | 判断最多N位小数 默认两位 |
| [isNumCode](#isnumcode)     | Boolean | 是否数字验证码 默认六位  |
| [isNumNumber](#isnumnumber) | Boolean | 判断是否数字        |
| [isNumInt](#isnumint)       | Boolean | 判断是否整数        |
| [isNumPosi](#isnumposi)     | Boolean | 判断是否正整数       |
| [isNumZS](#isnumzs)         | Boolean | 验证是否正数        |
