# 检测常用

### 方法概览

| 方法                            | 返回   | 说明                     |
| :------------------------------ | :----- | :----------------------- |
| [isBank](#isbank)               | Boolean | 银行卡                 |
| [isPostal](#ispostal)           | Boolean | 邮编                  |
| [isEmail](#isemail)             | Boolean | 邮箱                  |
| [isMobile](#ismobile)           | Boolean | 手机号                 |
| [isLandline](#islandline)       | Boolean | 固定电话（ xxxx-xxxxxx ） |
| [isUrlHttp](#isurlhttp)         | Boolean | URL地址               |
| [isUrl](#isurl)                 | Boolean | url地址               |
| [isEmoji](#isemoji)             | Boolean | emoji表情             |
| [isEmpty](#isempty)             | Boolean | 为空                  |
| [isRequire](#isrequire)         | Boolean | 空                   |
| [isEmptyTrim](#isemptytrim)     | Boolean | 去掉前后空格，是否为空         |
| [isEmptyObject](#isemptyobject) | Boolean | 为空对象                |
| [isHave](#ishave)               | boolean | 判断是否有               |
