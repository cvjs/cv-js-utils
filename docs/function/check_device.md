# 检测设备驱动

### 方法概览

| 方法                                      | 返回   | 说明                     |
| :---------------------------------------- | :----- | :----------------------- |
| [isDevice_mobile](#isdevice_mobile)       | Boolean | 是否 移动端     |
| [isDevice_ios](#isdevice_ios)             | Boolean | 是否 IOS     |
| [isDevice_iphone](#isdevice_iphone)       | Boolean | 是否 iphone  |
| [isDevice_ipad](#isdevice_ipad)           | Boolean | 是否 iPad    |
| [isDevice_android](#isdevice_android)     | Boolean | 是否 andorid |
| [hasDevice_internet](#hasdevice_internet) | Boolean | 是否 有网络权限   |
