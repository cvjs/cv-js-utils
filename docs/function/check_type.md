# 检测数据类型

### 方法概览

| 方法                                  | 返回   | 说明                     |
| :------------------------------------ | :----- | :----------------------- |
| [isTypeArray](#istypearray)           | Boolean | 是否数组       |
| [isTypeBoolean](#istypeboolean)       | Boolean | 是否布尔       |
| [isTypeDate](#istypedate)             | Boolean | 是否日期       |
| [isTypeFunction](#istypefunction)     | Boolean | 是否方法       |
| [isTypeNull](#istypenull)             | Boolean | 是否为空       |
| [isTypeNumber](#istypenumber)         | Boolean | 是否数字       |
| [isTypeObj](#istypeobj)               | Boolean | 是否对象       |
| [isTypeString](#istypestring)         | Boolean | 是否字符串      |
| [isTypeUndefined](#istypeundefined)   | Boolean | 是否未定义      |
| [isTypeRegExp](#istyperegexp)         | Boolean | 是否正则表达式    |
| [isTypeJsonString](#istypejsonstring) | Boolean | 是否json数据   |
| [isTypeJson](#istypejson)             | Boolean | 是否json数组对象 |
