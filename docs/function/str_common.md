# 字符串常用

### 方法概览

| 方法                              | 返回   | 说明                     |
| :-------------------------------- | :----- | :----------------------- |
| [strTrimSide](#strtrimside)       | String | 去掉字符串左右空格 |
| [strTrimAll](#strtrimall)         | String | 去除字符串所有空格 |
| [strLenLimit](#strlenlimit)       | Number | 获取文本长度    |
| [strFeed](#strfeed)               | String | 文本换行      |
| [strExists](#strexists)           | Boolean | 字符串是否包含   |
| [strLeftExists](#strleftexists)   | Boolean | 字符串是否左边包含 |
| [strLeftDelete](#strleftdelete)   | String | 删除左边字符串   |
| [strRightExists](#strrightexists) | Boolean | 字符串是否右边包含 |
| [strRightDelete](#strrightdelete) | String | 删除右边字符串   |
| [str_ends_with](#str_ends_with)   | Boolean | 判断结尾      |
