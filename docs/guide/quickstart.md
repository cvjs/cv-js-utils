# 快速开始


# cv-js-utils

> js 常用工具类函数

## 使用方法 - 网页直接调用

```html
<script src="https://10ui.cn/@10yun/cv-js-utils/lib/cvUtils.min.js"></script>
<script>console.log(cvUtils.isMobile('15060007000'))</script>
```

## 使用方法 - import 调用

```sh
npm install @10yun/cv-js-utils -f
```

- 所有引用
  
```js
import cvUtils from '@10yun/cv-js-utils';
// 或
const cvUtils = require('@10yun/cv-js-utils');
console.log( cvUtils.isMobile('150****7000') );// 验证手机
```

- 按需引用

```javascript
import { isMobile } '@10yun/cv-js-utils';
// 或
const isMobile = require('@10yun/cv-js-utils');
console.log( isMobile('150****7000') );// 验证手机
```
