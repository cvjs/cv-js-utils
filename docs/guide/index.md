# 关于

### 常用函数


### 贡献代码

在使用 `cvjs` 中，如遇到无法解决的问题，请提 [Issues](https://gitee.com/cvjs/cv-js-utils/issues) 给我们；  
假如您有更好的点子或更好的实现方式，也欢迎给我们提交 [PR](https://gitee.com/cvjs/cv-js-utils/pulls)