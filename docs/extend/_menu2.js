export default [
  { text: '正则', link: '/function_check/正则' },
  {
    text: '判断验证',
    collapsed: false,
    items: [
      { text: '校验 常用', link: '/function_check/common' },
      { text: '校验 设备、浏览器', link: '/function_check/device' },
      { text: '校验 数据类型', link: '/function_check/type' },
      { text: '校验 数字', link: '/function_check/check_num' },
      { text: '校验 字符串', link: '/function_check/check_str' }
    ]
  },
  { text: '介绍', link: '/function/' },
  { text: '基础', link: '/function/base' },
  { text: '金额', link: '/function/amount' },
  { text: '字符串', link: '/function/str' },
  { text: '数字', link: '/function/number' },
  { text: '数组', link: '/function/array' },
  { text: '随机数', link: '/function/random' },
  { text: '位置', link: '/function/geo' },
  { text: 'URI', link: '/function/uri' },
  { text: '颜色', link: '/function/color' }
];
