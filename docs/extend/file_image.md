

## 文件 图片

| 方法            | 返回    | 说明                |
| :-------------- | :------ | :------------------ |
| fileCheckImg    | Boolean | 判断是否是图片      |
| fileImgCompress |         | 压缩图片            |
| fileGetExt      |         | 获取文件后缀 格式名 |


```js
// base64位码转blob对象
@params dataurl - dataUrl
dataURLtoBlob(dataUrl)
```

```js  
// 将base64/dataurl转成File
@params dataurl - dataUrl, filename
dataURLtoFile(dataUrl, filename)
```

```js  
// 获取File 对象或 Blob 对象的临时路径
@params file - File/Blob对象
getObjectURL(file)
```



### fileGetExt

```js
fileGetExt("sabc.png"); // png
```

### fileImgCompress

```js
/**
  *   压缩图片
  *   dataUrl： dataUrl，
  *   obj：{ height, width, quality } 压缩之后的图片宽高，图片质量（0-1）
  *   type：压缩完之后的图片类型
  */
fileImgCompress(dataUrl, { height:300,width:300,quality:1 },"image/png")
```
