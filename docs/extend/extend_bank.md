# 银行字典


- [ Bank 模块 ]  
- Bank未内置在Utils模块里，需要单独引入

```js
import Bank from '@10yun/cv-js-utils/extend/bankident'


// 银行所属识别
bankCodeGetName('6227 0018 2000 3000 4000')

// 用于输入银行卡号进行搜索归属银行
bankCardAttribution('6227 0018 2000 3000 4000')

```