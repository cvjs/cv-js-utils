---
layout: home

title: cvjs
titleTemplate: 选项卡描述
editLink: true
lastUpdated: true

hero:
  name: cvjs
  text: 快速使用，专注业务
  tagline: 复制黏贴，极简使用
  image:
    src: /logo-nobg.png
    alt: cvjs
  actions:
    - theme: brand
      text: 快速开始
      link: /guide/
    - theme: alt
      text: 为什么选 cvjs？
      link: /guide/why
    - theme: alt
      text: 在 Gitee 上查看
      link: https://gitee.com/cvjs/cv-js-utils
    - theme: alt
      text: 在 Github 上查看
      link: https://github.com/10yun
features:
  - icon: 💡
    title: 简洁至上
    details: 精简结构，复制黏贴调用就行
  - icon: ⚡️
    title: Vue驱动
    details: 享受 cv-ui 的开发体验，使用 Vue 组件，同时可以使用 Vue 来开发自定义主题。
  - icon: 🔨
    title: 功能/特点 1
    details: 持续迭代，功能全面。
  - icon: 🛠️
    title: 丰富的功能
    details: 对 TypeScript、JSX、CSS 等支持开箱即用。
  - icon: 📦
    title: 优化的构建
    details: 可选 “多页应用” 或 “库” 模式的预配置 Rollup 构建
  - icon: 🔩
    title: 通用的插件
    details: 在开发和构建之间共享 Rollup-superset 插件接口。
  - icon: 🔑
    title: 完全类型化的API
    details: 灵活的 API 和完整的 TypeScript 类型。
  - icon: 🧩
    title: 功能全面
    details: 功能/特点 2 具体描述信息。
  - icon: ✈️
    title: 功能/特点 3。
    details: 功能/特点 3 具体描述信息。

---
<!-- 
<script setup>
import { onMounted } from 'vue'
import { fetchReleaseTag } from './.vitepress/utils/fetchReleaseTag.js'

onMounted(() => {
  fetchReleaseTag()
})
</script> 
-->