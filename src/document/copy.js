/*
 * 
 * 
 * 
 * @js无法复制
 @你绑定的 .get-btn元素并不是button ,input吧?
 @如果是div的苹果默认不能点击的元素 , 在CSS上加上cursor: pointer;
 @苹果就有这个 bug 不是可点击元素的动态绑定click就无效~
 @已经测试到原因了。在IOS上复制的文本必须是可视状态，才能够复制~我把那个文本input给hidden了就不行，
 @Ios上，要拷贝的文本需要在可视状态
 @在你点击的元素上加一个空点击事件：onclick=""。因为ios不单纯支持on
 @https://www.aliyun.com/jiaocheng/362384.html
 @https://stackoverflow.com/questions/34045777/copy-to-clipboard-using-javascript-in-ios 
 * 
 */

/*
 *
 * Note that the el parameter to this function must be an <input> or a <textarea>.
 * 请注意，EL参数这个函数必须是<输入>或<文本>。
 */
function iosCopyToClipboard(el) {
  var oldContentEditable = el.contentEditable;
  var oldReadOnly = el.readOnly;
  var range = document.createRange();

  el.contenteditable = true;
  el.readonly = false;
  range.selectNodeContents(el);
  var s = window.getSelection();
  s.removeAllRanges();
  s.addRange(range);
  el.setSelectionRange(0, 999999); // A big number, to cover anything that could be inside the element.
  el.contentEditable = oldContentEditable;
  el.readOnly = oldReadOnly;
  document.execCommand('copy');
}

/*
 * I've searched for some solutions and I've found one that actually works 。 我搜索了一些解决方案，我发现一个真正的作品：
 * 
 * http://www.seabreezecomputers.com/tips/copy2clipboard.htm 。
 * 
 * 基本上，示例可能类似于：
 * 
 */
var $input = $(' some input/textarea ');
$input.val(result);
if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
  var el = $input.get(0);
  var editable = el.contentEditable;
  var readOnly = el.readOnly;
  el.contentEditable = true;
  el.readOnly = false;
  var range = document.createRange();
  range.selectNodeContents(el);
  var sel = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
  el.setSelectionRange(0, 999999);
  el.contentEditable = editable;
  el.readOnly = readOnly;
} else {
  $input.select();
}
document.execCommand('copy');
$input.blur();

/* ========= */

var clipboard = new Clipboard('.copyUrl');
// 兼容ios复制
$('.copyUrl').on('click', function () {
  var $input = $('#copyIos');
  $input.val(share_url);
  if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
    clipboard.on('success', function (e) {
      e.clearSelection();
      $.sDialog({
        skin: "red",
        content: 'copy success!',
        okBtn: false,
        cancelBtn: false,
        lock: true
      });
      console.log('copy success!');
    });
  } else {
    $input.select();
  }
  // document.execCommand('copy');
  $input.blur();
});