/* @action: 选中和取消选中 */
function ctoCheckboxAll(o, n) {
  o = o || 'ids[]';
  n = n || 0;
  var i,
    str = document.getElementsByName(o),
    len = str.length,
    chestr = '';
  for (i = 0; i < len; i++) {
    if (n == 1) {
      if (str[i].checked) {
        chestr += (chestr == '' ? '' : ',') + str[i].value;
      }
    } else {
      if (str[i].checked) {
        str[i].checked = false;
      } else {
        str[i].checked = true;
      }
    }
  }
  return chestr;
}

/* @action: 复选框的选择与获取值 */
function ctoCheckboxGet(o) {
  return ctoCheckboxAll(o, 1);
}
export class CheckClass {}
