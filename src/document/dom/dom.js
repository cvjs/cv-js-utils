import CheckClass from './check.js';
class DomClass {
  /**
   * 使用方法
   * DomObj.input().xxxx('#dom-name')
   */
  input() {
    // return new InputClass();
  }
  /**
   * 使用方法
   * DomObj.check().checkAll('#dom-name')
   */
  check() {
    return new CheckClass();
  }
}
export default DomClass;
