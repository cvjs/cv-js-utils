;
(function ($) {
  /**
   * @action 表单序列化成json
   * @author ctocode-zwj
   * @version 2018-12-04
   */
  $.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
      if (o[this.name] !== undefined) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };
  /**
   * @action: 多个表单合并成一个数组
   * @author ctocode-zhw
   * @version 1.0.0.20161108_Beta
   * @link http://www.ctocode.com 提供
   * @copyright ctocode
   * @使用方法:
   * @1:var a_form = $("#aForm").serializeJson();
   * @2:var b_form = $('#bForm').serializeJson();
   * @3:postparam = $.extend(a_form,b_form);
   * @$.ajax({ data : postparam });
   */
  $.fn.serializeJson = function () {
    var serializeObj = {};
    var array = this.serializeArray();
    var str = this.serialize();
    $(array).each(function () {
      if (serializeObj[this.name]) {
        if ($.isArray(serializeObj[this.name])) {
          serializeObj[this.name].push(this.value);
        } else {
          serializeObj[this.name] = [serializeObj[this.name], this.value];
        }
      } else {
        serializeObj[this.name] = this.value;
      }
    });
    return serializeObj;
  };
})($);
