"use strict";

var ccjs = window.ctocode && ctocode.define;

/**
 * @action 加载ctocode.js所需要的主文件
 * @author ctocode-zhw
 * @version 1.0.0.20170120_Beta
 * @copyright Copyright (c) 2016 ~, 10yun.com, Inc.
 * @link https://www.10yun.com 提供
 */
function consoleShowInfo() {
  var e = window.console;
  if (e && e.log) {
    var t = "%c";
    var a = "color:#008B45;font-weight:bolder;font-size:25px;";
    a += "text-shadow:15px 15px 5px #fff, 15px 15px 10px #373E40, 15px 15px 15px #A2B4BA, 15px 15px 30px #82ABBA;";
    var n = "color:#495A80;text-shadow: 0 1px 0 #ccc,0 2px 0 #c9c9c9,0 1px 0 #bbb;font-size:20px";
    e.info('\n', '~~~~~~~华丽的分割线~~~~~~');
    console_html = t + "\n";
    console_html += " 十云 ~ 等着你的加入~ ";
    console_html += " \n";
    console_html += " \n";
    e.info(console_html, a);
    e.info(t + "一不小心被你发现了:-)\r\n什么都不说了,这里需要喜欢探寻秘密的你!\r\n【十云】网址:https://www.10yun.com/", n);
  }
}
// 自动加载ctocode组件
function consoleInfoReady() {
  console.log("欢迎使用ctoui，如果您在使用的过程中有碰到问题，可以参考开发文档，感谢您的支持。")
}

// 判断传入的是id，class，还是其他
// doc = document,
// regId = /^#[\w\-]+/,
// regCls = /^([\w\-]+)?\.([\w\-]+)/,
// regTag = /^([\w\*]+)$/,
// regNodeAttr = /^([\w\-]+)?\[([\w]+)(=(\w+))?\]/;

function domGetScriptPath() {
  var e = document.currentScript ? document.currentScript.src : function () {
    for (var e, t = document.scripts, n = t.length - 1, a = n; a > 0; a--)
      if ("interactive" === t[a].readyState) {
        e = t[a].src;
        break
      }
    return e || t[n].src
  }();
  return e.substring(0, e.lastIndexOf("/") + 1)
};

/* 判断文件是否存在 */
function domIsInclude(name) {
  name = name || '';
  if (!name || name.length === 0) {
    throw new Error('argument "path" is required !');
    return false;
  }
  var js = /js$/i.test(name);
  var es = document.getElementsByTagName(js ? 'script' : 'link');
  for (var i = 0; i < es.length; i++)
    if (es[i][js ? 'src' : 'href'].indexOf(name) != -1)
      return true;
  return false;
}

function domLoadCss(fileName, fileType, callFn) {
  var _self = this;
  if (domIsInclude(_self.scriptDomainUrl + fileName) == true) {
    return false;
  }
  var file_url = _self.scriptDomainUrl + fileName;
  var file_app = file_url.replace(/\.|\//g, '');
  var file_id = 'ctoui-' + file_app;
  file_url = file_url + (_self.config.debug ? '?v=' + new Date().getTime() : '');

  var fileUi = document.createElement("link");
  fileUi.setAttribute("rel", "stylesheet"); // link.rel = 'stylesheet';
  fileUi.setAttribute("type", "text/css");
  fileUi.setAttribute("href", file_url);
  // link.media = 'all';
  // link.id = ,
  if (!document.getElementById(file_id)) {
    fileUi.setAttribute("id", file_id);
    document.getElementsByTagName("head")[0].appendChild(fileUi);
  }
  return true;
}
function domLoadJs(fileName, fileType, callFn) {
  var _self = this;
  if (domIsInclude(_self.scriptDomainUrl + fileName) == true) {
    return false;
  }
  var file_url = _self.scriptDomainUrl + fileName;
  var file_app = file_url.replace(/\.|\//g, '');
  var file_id = 'ctoui-' + file_app;
  file_url = file_url + (_self.config.debug ? '?v=' + new Date().getTime() : '');

  var fileUi = document.createElement('script');/* 创建标签 */
  fileUi.setAttribute("type", "text/javascript");/* 定义属性type的值为text/javascript */
  fileUi.setAttribute("src", file_url);/* 文件的地址 */
  if (!document.getElementById(file_id)) {
    fileUi.setAttribute("id", file_id);
    document.getElementsByTagName("head")[0].appendChild(fileUi);
  }
  return true;
}
function domLoadJs(path, callback) {
  if (!path || path.length === 0) {
    throw new Error('argument "path" is required !');
  }
  /* 获取现在已经加载的所有script */
  var scriptArr = document.getElementsByTagName('script');
  for (var i = 0; i < scriptArr.length; i++) {
    if (scriptArr[i].src.lastIndexOf(path) != -1) {
      return;
    }
  }
  var script = document.createElement('script');
  script.type = 'text/javascript';
  if (script.readyState) { // IE
    script.onreadystatechange = function () {
      if (script.readyState == "loaded" || script.readyState == "complete") {
        script.onreadystatechange = null;
        if (callback != undefined) {
          callback();// 回调方法
        }
      }
    };
  } else { // Others
    script.onload = function () {
      script.onload = null;
      if (callback != undefined) {
        callback();// 回调方法
      }
    };
  }
  script.src = _CTO_ROOT_ + path;
  var head = document.getElementsByTagName('head')[0];
  head.appendChild(script);
  // for ( var i = 0 in scriptArr) {
  // var scriptSrc = scriptArr[i].src;// .getAttribute("src");
  // 
  // console.log(scriptSrc, path, scriptSrc.indexOf(path));
  // if (scriptSrc.indexOf(path) <= -1) {
  //
  // return false;
  // }
  //
  // }
}
function domGetStyle(e, t) {
  var n = e.currentStyle ? e.currentStyle : window.getComputedStyle(e, null);
  return n[n.getPropertyValue ? "getPropertyValue" : "getAttribute"](t)
}

/* @action: 选中和取消选中 */
function domCheckAll(o, n) {
  o = o || 'ids[]';
  n = n || 0;
  var i, str = document.getElementsByName(o), len = str.length, chestr = '';
  for (i = 0; i < len; i++) {
    if (n == 1) {
      if (str[i].checked) {
        chestr += (chestr == '' ? '' : ',') + str[i].value;
      }
    } else {
      if (str[i].checked) {
        str[i].checked = false;
      } else {
        str[i].checked = true;
      }
    }
  }
  return chestr;
}

/* @action: 复选框的选择与获取值 */
function domCheckGet(o) {
  return ctoCheckAll(o, 1);
}
