/**
 * 数组相关 -
 * @parasm {Array}  o 重新处理 主要是推荐这个函数。
 * @returns {Object} v 它将jquery系列化后的值转为name:value的形式。
 */
function jqFormToData(o) {
  var v = {};
  for (var i in o) {
    if (typeof v[o[i].name] == 'undefined') {
      v[o[i].name] = o[i].value;
    } else {
      v[o[i].name] += ',' + o[i].value;
    }
  }
  return v;
}
// var formdiyArrResult2 = classObj.diaObj.find('form').serializeArray();
// console.log(formdiyArrResult2);
// var postData = jqFormToData(formdiyArrResult2);
// postData.postType = "update";
// postData.handleType = "xxx";

function doLVTransmitParamFlag(pageFlag) {
  // console.log('old-', pageFlag);
  var cbFlag = CONSOLE_BASE_FLAG || '';
  if (cbFlag !== '') {
    pageFlag = pageFlag.replace(cbFlag, '');
  }
  pageFlag = pageFlag
    .replace(/\/\//g, '/') // 双斜杠改单斜杠
    .replace(/\/\//g, '/') // 双斜杠改单斜杠
    .replace(/\/\//g, '/') // 双斜杠改单斜杠
    // .replace(new RegExp("\/\/","gm"),"/")
    .replace(/(^\/)|(\/$)/, '') // 去掉开头结尾斜杠
    .replace(/\//g, '_'); // 斜杠转下划线
  // console.log('new-', pageFlag);
  return pageFlag;
}
// 获取 layui + vue 页面传输数据
function getLVTransmitParam(pageFlag, noDel) {
  noDel = noDel || false; // 是否删除
  pageFlag = pageFlag || ''; // 页面表示
  pageFlag = doLVTransmitParamFlag(pageFlag);
  // 取缓存
  var paramArr = window.sessionStorage.getItem(`${pageFlag}`) || {};
  // console.log(pageFlag,paramArr);
  if (Object.keys(paramArr).length == 0 || JSON.stringify(paramArr) == '{}') {
    return {};
  } else {
    if (noDel) {
    } else {
      // 取缓存，返回
      sessionStorage.removeItem(pageFlag);
    }
    return JSON.parse(paramArr);
  }
}
// 设置 layui + vue 页面传输数据
function setLVTransmitParam(url) {
  // 自动获取地址栏链接带？以及后面的字符串
  // var url = window.location.search;
  if (url == '') {
    return;
  }
  var openURLArr = new URL(url);
  var pageFlag = openURLArr.pathname;
  pageFlag = doLVTransmitParamFlag(pageFlag);

  var paramStr = openURLArr.search;
  // 定义一个空对象
  var paramArr = {};
  // 如果字符串里面存在?
  if (paramStr.indexOf('?') != -1) {
    // 如果存在&符号，则再以&符号进行分割
    var params = paramStr.split('?')[1].split('&');
    params.map(function (v) {
      paramArr[v.split('=')[0]] = v.split('=')[1];
    });
    // 遍历数组
    // for (var i = 0; i < arr.length; i++) {
    // // obj对象的属性名 = 属性值，unescape为解码字符串
    // paramArr[arr[i].split("=")[0]] = unescape(arr[i].split("=")[1]);
    // }
  }
  // console.log(pageFlag, paramArr);
  if (Object.keys(paramArr).length == 0 || JSON.stringify(paramArr) == '{}') {
  } else {
    // 存入缓存
    window.sessionStorage.setItem(`${pageFlag}`, JSON.stringify(paramArr));
    return pageFlag;
  }
}
