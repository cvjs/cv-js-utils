//dog是一个方法类 就是一个简短的自定义的类jquery库
var dog = {
  JQ: function (id) {
    // return document.querySelector(id);
  },
  trim(x) {
    return x.replace(/^\s+|\s+$/gm, '');
  },
  isHidden(el) {
    var style = (window.getComputedStyle() && document.getComputedStyle())(el);
    return style.display === 'none';
  },
  tpl: function (html, data) {
    return html.replace(/{{(\w+)}}/g, function (item, a) {
      return data[a];
    });
  },
  pos: function (obj, attr) {
    if (obj) {
      return obj.getBoundingClientRect()[attr];
    } else {
      alert('对象不存在！');
    }
  },
  viewSize: (function () {
    var de = document.documentElement;
    var doc = document;
    return {
      width: window.innerWidth || (de && de.clientWidth) || doc.body.clientWidth,
      height: window.innerHeight || (de && de.clientHeight) || doc.body.clientHeight
    };
  })(),
  on: function (el, type, handler) {
    el.addEventListener(type, handler, false); //监听事件
  },
  off: function (el, type, handler) {
    el.removeEventListener(type, handler, false); //移除监听
  },
  css: function (obj, attr) {
    if (obj.currentStyle) {
      return obj.currentStyle[attr];
    } else {
      return getComputedStyle(obj, false)[attr];
    }
  },
  act: function (obj, attr, target) {
    var that = this;
    clearInterval(obj.timer);
    obj.timer = setInterval(function () {
      var stop = true;
      var cur = parseInt(that.css(obj, attr));
      var speed = (target - cur) / 8;
      speed = speed < 0 ? Math.ceil(speed) : Math.floor(speed);
      if (target != speed) {
        stop = false;
      }
      obj.style[attr] = speed + cur + 'px';
      if (stop) {
        clearInterval(obj.timer);
      }
    }, 55);
  },
  //自己实现each方法
  each: function (arr, callback) {
    //数组，回调函数 arr大约等于opt
    if (Array.isArray(arr)) {
      for (var i = 0, l = arr.length; i < l; i++) {
        //出现false即出错情况下出现
        if (callback.call(arr[i], i, arr[i++]) == false) {
          //元素对象存在 i相当于下面function(i,m)中的i,arr[i++]相当于m
          break;
        }
      }
    } else {
      for (var name in arr) {
        if (callback.call(arr[name], name, arr[name]) == false) {
          //元素对象存在 name相当于下面function(i,m)中的i,arr[name]相当于m
          break;
        }
      }
    }
  },
  create: function (opt) {
    //传入一个对象
    var el = document.createElement(opt.tag || 'div');
    this.each(opt, function (i, m) {
      el[i] = this; //
    });
    return el;
  }
};

export default dog;
