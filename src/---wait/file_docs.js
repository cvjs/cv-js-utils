// 使用方式
// this.tmepListDatas = DocFunc.getDirAllTree('php');
function getDirAllTree(dirName) {
  dirName = dirName || '';
  if (dirName == '') {
    return [];
  }
  let dirPath = '/static/docs/' + dirName;
  // 拉取该文件夹下所有文件信息
  const filesMDArr = require.context('@/static/docs', true, /\.md$/);
  const filesMDNames = filesMDArr.keys();
  var tmepListTree = [];
  // const last
  filesMDNames.map((item) => {
    // 剥去文件名开头的 `./`
    const item2 = item.replace(/^\.\//, '');

    var splitpath = item2.replace(/^\/|\/$/g, '').split('/');
    var ptr = tmepListTree;
    for (let i = 0; i < splitpath.length; i++) {
      let node = {
        name: splitpath[i],
        type: 'directory',
        showChild: false
      };
      if (i == splitpath.length - 1) {
        // node.size = obj.size;
        node.type = 'file';
        // fileName.replace(/\.md/, ''); 和`.md$`结尾的扩展名
        node.name = splitpath[i].replace('.md', '');
        node.url = node.name + '.html';
      }
      // if (pathArrItem.indexOf('.md') !== -1) { }
      let Index = ptr.findIndex((item) => {
        return item.name === splitpath[i];
      });
      if (Index === -1) {
        ptr.push(node);
        ptr[ptr.length - 1]['children'] = [];
      } else {
        ptr[Index].children = ptr[Index].children;
      }
      let index = Index > -1 ? Index : ptr.length - 1;
      ptr = ptr[index].children;
    }
  });
  return tmepListTree;
}

export { getDirAllTree };
