/**
 * @title uri
 */

import { strRightDelete, strExists } from './str_common.js';
import { strGetMiddle } from './str_replace.js';
/**
 * @action 正则提取域名
 * @param weburl
 * @returns {String}
 */
function urlGetDomain(weburl) {
  let urlReg = /http(s)?:\/\/([^\/]+)/i;
  let domain = (weburl + '').match(urlReg);
  return domain != null && domain.length > 0 ? domain[2] : '';
}

/**
 * 指定键获取url参数
 * @param key
 * @returns {Mixed}
 */
function urlParameter(key) {
  let params = urlGetParameterAll();
  return typeof key === 'undefined' ? params : params[key];
}
function urlGetParameterAll() {
  let search = window.location.search || window.location.hash || '';
  let arr = [];
  if (strExists(search, '?')) {
    arr = strGetMiddle(search, '?').split('&');
  }
  let params = {};
  for (let i = 0; i < arr.length; i++) {
    let data = arr[i].split('=');
    if (data.length === 2) {
      params[data[0]] = data[1];
    }
  }
  return params;
}

/**
 * @action 删除地址中的参数
 * @param url
 * @param parameter
 * @returns {String}
 */
function urlRemoveParameter(url, parameter) {
  if (parameter instanceof Array) {
    parameter.forEach((key) => {
      url = urlRemoveParameter(url, key);
    });
    return url;
  }
  let urlparts = url.split('?');
  if (urlparts.length >= 2) {
    //参数名前缀
    let prefix = encodeURIComponent(parameter) + '=';
    let pars = urlparts[1].split(/[&;]/g);

    //循环查找匹配参数
    for (let i = pars.length; i-- > 0; ) {
      if (pars[i].lastIndexOf(prefix, 0) !== -1) {
        //存在则删除
        pars.splice(i, 1);
      }
    }

    return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
  }
  return url;
}
/**
 * @action 连接加上参数
 * @param url
 * @param params
 * @returns {Mixed}
 */
function urlAddParams(url, params) {
  // if (typeof params === 'object' && params !== null) {
  if (
    typeof params == 'object' &&
    Object.prototype.toString.call(params).toLowerCase() == '[object object]' &&
    typeof params.length == 'undefined'
  ) {
    if (url) {
      url = urlRemoveParameter(url, Object.keys(params));
    }
    url += '';
    url += url.indexOf('?') === -1 ? '?' : '';
    for (let key in params) {
      if (!params.hasOwnProperty(key)) {
        continue;
      }
      url += '&' + key + '=' + params[key];
    }
  }
  // return url.replace('?&', '?');
  return strRightDelete(url.replace('?&', '?'), '?');
}
/**
 * @action 获取uri参数
 * @param {*} name
 * @returns {Object}
 */
// 获取 URL 地址栏参数
function uriGetParam(name) {
  // 构造一个含有目标参数的正则表达式对象
  var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
  // 匹配目标参数
  // 使用 slice 方法获取查询字符串（去掉开头的问号）
  var queryString = window.location.search.slice(1);
  // 使用 match 方法对查询字符串进行匹配
  var r = queryString.match(reg);
  // window.location.search.substring(1);
  if (r != null) {
    return decodeURIComponent(r[2]); //返回参数值
  }
  let queryObj = urlQueryToObj();
  if (queryObj[name]) {
    return queryObj[name];
  }
  return null; //返回参数值
}

/**
 * @action url转成对象
 * @param String url
 * @description 从URL中解析参数
 * @returns {Object}
 */
function urlQueryToObj(url) {
  // url = '';
  url = decodeURIComponent(url);

  let paramObj = {};
  const pararmNeed = url.split('?')[1];
  if (!pararmNeed) {
    return {};
  }
  const pararmArr = pararmNeed.split('&');
  for (let i in pararmArr) {
    let paramItem = pararmArr[i];
    const itemArr = paramItem.split('=');
    paramObj[itemArr[0]] = itemArr[1];
  }
  return paramObj;
}

/**
 * 对象转url参数
 */
function __objToQueryFilter(str) {
  // 特殊字符转义
  str += ''; // 隐式转换
  str = str
    .replace(/%/g, '%25')
    .replace(/\+/g, '%2B')
    .replace(/ /g, '%20')
    .replace(/\//g, '%2F')
    .replace(/\?/g, '%3F')
    .replace(/&/g, '%26')
    .replace(/\=/g, '%3D')
    .replace(/#/g, '%23');
  return str;
}
/**
 * @action 对象转成url查询字符串
 * @param {Object} paramObj 参数对象
 * @param {String} isPrefix 链接前缀
 * @param {Object} query 参数对象
 * @param {String} url 链接地址
 * @param {Boolean} isSequence 是否对query参数进行 json.stringify
 * @returns {String}
 */
function urlObjToQuery(paramObj, isPrefix = false, isSequence = true) {
  isPrefix = isPrefix ? isPrefix : false;
  let prefix = isPrefix ? '?' : '';
  let sdata = [];
  for (let key in paramObj) {
    let itemObj = paramObj[key];
    let itemQuery = `${key}=${__objToQueryFilter(itemObj)}`;
    // let itemQuery = `${key}=${encodeURIComponent(isSequence ? JSON.stringify(itemObj) : itemObj)}`;
    sdata.push(itemQuery);

    // // 去掉为空的参数
    // if (['', undefined, null].includes(value)) {
    //   continue;
    // }
    // if (value.constructor === Array) {
    //   value.forEach((_value) => {
    //     sdata.push(encodeURIComponent(key) + '[]=' + encodeURIComponent(_value));
    //   });
    // } else {
    //   sdata.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
    // }
  }
  return sdata.length ? prefix + sdata.join('&') : '';
}

export {
  urlGetDomain,
  urlParameter,
  urlGetParameterAll,
  urlRemoveParameter,
  urlAddParams,
  uriGetParam,
  urlQueryToObj,
  urlObjToQuery
};
