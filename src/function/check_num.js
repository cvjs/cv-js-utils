/**
 * @title 检测数字
 */

/**
 * @action 判断最多N位小数 默认两位
 * @param {*} number
 * @param {*} length
 * @returns {Boolean}
 */
function isDecimal(number, length = 2) {
  var reg = `/^(([1-9]{1}\\d*)|(0{1}))(\\.\\d{1,${length}})?$/`;
  return reg.test(number);
}
/**
 * @action 是否数字验证码 默认六位
 * @param {*} str
 * @param {*} length
 * @returns {Boolean}
 */
function isNumCode(str, length = 6) {
  var reg = `/[0-9]{${length}}$/`;
  return reg.test(str);
}
/**
 * @action 判断是否数字
 * @desc +2 -1，也为真 @param {Object} val
 * @param {*} val
 * @returns {Boolean}
 */
function isNumNumber(val) {
  let value = val.toString();
  return value.search(/^[+-]?[0-9.]*$/) >= 0;
  // return /^[+\-]?\d+(\.\d+)?$/.test(val)
}
/**
 * @action 判断是否整数
 * @param {*} val
 * @returns {Boolean}
 */
function isNumInt(val) {
  return Math.floor(val) === val;
}
/**
 * @action 判断是否正整数
 * @param {*} val
 * @returns {Boolean}
 */
function isNumPosi(val) {
  if (val == '') return false;
  var re = /^[1-9]\d*$/;
  return re.test(val);
}
// 是否正整数ID
function isNumPosi2(val) {
  return /^[\d]{15}$/.test(val) || /^[\d]{17}([Xx\d]{1}$)$/.test(val);
}
/**
 * @action 验证是否正数
 * @returns {Boolean}
 */
function isNumZS(num) {
  var reg = /^\d+(?=\.{0,1}\d+$|$)/;
  if (reg.test(num)) return true;
  return false;
}
export { isDecimal, isNumNumber, isNumInt, isNumPosi, isNumZS, isNumCode };
