/**
 * @title 检测常用
 */

function _strTrim(str) {
  return str.replace(/\s+/g, '');
}
/**
 * @action 银行卡
 * @param {*} str
 * @returns {Boolean}
 */
function isBank(str) {
  let reg = /^[0-9]{12,}$/;
  str = str ? _strTrim(str) : '';
  return reg.test(str);
}
/**
 * @action 邮编
 * @param {*} str
 * @returns {Boolean}
 */
function isPostal(str) {
  let reg = /^[0-9]{6}$/;
  str = str ? _strTrim(str) : '';
  return reg.test(str);
}
/**
 * @action 邮箱
 * @param {*} str
 * @returns {Boolean}
 */
function isEmail(val) {
  // let reg = /^[0-9a-z_][_.0-9a-z-]{0,31}@([a-z0-9][-a-z0-9]{0,30}\.){1,4}[a-z]{2,4}$/;
  //         "^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$"
  let reg4 = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*\.)+[a-zA-Z]*)$/i;
  let reg3 = /^[-a-z0-9\._]+@([-a-z0-9\-]+\.)+[a-z0-9]{2,3}$/i;
  let reg2 = /\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/g;
  let reg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
  return reg.test(val);
}
/**
 * @action 手机号
 * @param {*} str
 * @returns {Boolean}
 */
function isMobile(str) {
  str = str || '';
  if (str == '') {
    return false;
  }
  str = str ? _strTrim(str) : '';
  str = parseInt(str);
  let reg3 = /^1([3456789])\d{9}$/;
  let reg2 = /^((13[0-9]{1})|(14[5,7,9]{1})|(15[0-9]{1})|(17[035678]{1})|(18[0-9]{1}))+\d{8}$/;
  let reg = /^((13[0-9])|(14[5,7,9])|(15[^4])|(16[6])|(17[1,2,3,5,6,7,8])|(18[0-9])|(19[1,3,8,9]))+\d{8}$/;
  return reg.test(str) ? true : false;
}
/**
 * @action 固定电话（ xxxx-xxxxxx ）
 * @returns {Boolean}
 */
function isLandline(val) {
  val = val ? _strTrim(val) : '';
  let isPhone = /^([0-9]{3,4}-)?[0-9]{7,8}$/; // 座机号码
  let isMob = /^0?1[3|4|5|8][0-9]\d{8}$/; // 座机格式  区号之后用'-'隔开
  let isLandline = /^400[0-9]{7}$/; //400开头电话
  if (isMob.test(val) || isPhone.test(val) || isLandline.test(val)) {
    return true;
  }
  return false;
}
/**
 * @action URL地址
 * @demo isCheckUrl(location.href) || alert('请先部署到 localhost 下再访问');
 * @param {*} url
 * @returns {Boolean}
 */
function isUrlHttp(val) {
  val = val ? _strTrim(val) : '';
  let reg = `/^http|https:\/\/([\w-]+(\.[\w-]+)+(\/[\w-.\/\?%@&+=\u4e00-\u9fa5]*)?)?$/`;
  // let reg = /^http(s*):\/\//;
  // let reg = /^https?:\/\//i;
  return reg.test(val) ? true : false;
}
/**
 * @action url地址
 * @param {*} val
 * @returns {Boolean}
 */
function isUrl(val) {
  val = val ? _strTrim(val) : '';
  let reg = `/^(\w+:\/\/)?\w+(\.\w+)+.*$/`;
  return reg.test(val) ? true : false;
}
/**
 * @action emoji表情
 * @param {*} val
 * @returns {Boolean}
 */
function isEmoji(val) {
  const regStr =
    /[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF][\u200D|\uFE0F]|[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF]|[0-9|*|#]\uFE0F\u20E3|[0-9|#]\u20E3|[\u203C-\u3299]\uFE0F\u200D|[\u203C-\u3299]\uFE0F|[\u2122-\u2B55]|\u303D|[\A9|\AE]\u3030|\uA9|\uAE|\u3030/gi;
  return regStr.test(val);
}
/**
 * @action 为空
 * @param {*} parameter
 * @returns {Boolean}
 */
function isEmpty(parameter) {
  return undefined == parameter || null == parameter || '' == parameter;
}
/**
 * @action 空
 * @param {*} val
 * @returns {Boolean}
 */
function isRequire(val) {
  // /[\w\W]+/.test(val)
  val = val ? _strTrim(val) : '';
  return val.replace(/\s+/g, '') != '' ? true : false;
}
/**
 * @action 去掉前后空格，是否为空
 * @param {*} val
 * @returns {Boolean}
 */
function isEmptyTrim(val) {
  return /^\s*$/g.test(val.replace(/^\s+|\s+$/g, ''));
}
/**
 * @action 为空对象
 * @param {*} val
 * @returns {Boolean}
 */
function isEmptyObject(obj) {
  for (let i in obj) {
    return false;
  }
  return true;
}
/**
 * @action 判断是否有
 * @param str
 * @returns {boolean}
 */
function isHave(str) {
  return !!(str !== null && str !== 'null' && str !== undefined && str !== 'undefined' && str);
}
export {
  isBank,
  isEmail,
  isEmoji,
  isPostal,
  isMobile,
  isLandline,
  isUrlHttp,
  isUrl,
  isRequire,
  isEmpty,
  isEmptyTrim,
  isEmptyObject,
  isHave
};
