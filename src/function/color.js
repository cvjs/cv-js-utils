/**
 * @title 颜色
 */

/**
 * @action rgba转16进制的函数
 * @returns {String}
 */
function colorRgbaToHexify(values) {
  var a = parseFloat(values[3] || 1),
    r = Math.floor(a * parseInt(values[0]) + (1 - a) * 255),
    g = Math.floor(a * parseInt(values[1]) + (1 - a) * 255),
    b = Math.floor(a * parseInt(values[2]) + (1 - a) * 255);

  return '#' + ('0' + r.toString(16)).slice(-2) + ('0' + g.toString(16)).slice(-2) + ('0' + b.toString(16)).slice(-2);
}
/**
 * @action 随机生成rgb颜色
 * rgba参数的形成
 * @returns {String}
 */
function colorRandomRgba() {
  let r = Math.floor(Math.random() * 256);
  let g = Math.floor(Math.random() * 256);
  let b = Math.floor(Math.random() * 256);
  // let alpha = parseInt(Math.random()*10)/10;
  return `rgb(${r},${g},${b})`; //,${alpha}
}
/**
 * @action 根据图片生成一些相近颜色
 * color 画布
 * 获取对应图片背景rbg参数的函数
 * @param {*} img_rgb   图片颜色
 * @param {*} img_width 图片宽度
 * @param {*} img_height 图片高度
 * @returns {String}
 */
function colorToImgRbg(img_rgb, img_width, img_height) {
  // console.log(id)
  var r = 1,
    g = 1,
    b = 1, // 取所有像素的平均值
    data = img_rgb;
  for (var row = 0; row < img_height; row++) {
    for (var col = 0; col < img_width; col++) {
      // console.log(data[((img.width * row)+ col) * 4])
      if (row == 0) {
        r += data[img_width * row + col];
        g += data[img_width * row + col + 1];
        b += data[img_width * row + col + 2];
      } else {
        r += data[(img_width * row + col) * 4];
        g += data[(img_width * row + col) * 4 + 1];
        b += data[(img_width * row + col) * 4 + 2];
      }
    }
  }
  r /= img_width * img_height;
  g /= img_width * img_height;
  b /= img_width * img_height;
  // 将最终的值取整
  r = Math.round(r);
  g = Math.round(g);
  b = Math.round(b);
  // console.log(r, g, b)
  let arr = [Math.round(r), Math.round(g), Math.round(b)];
  // let yangshi ="rgb(" + r + "," + g + "," + b + ")"
  // console.log(arr)
  let rbg = colorRgbaToHexify(arr);
  return rbg;
}

export { colorRgbaToHexify, colorRandomRgba, colorToImgRbg };
