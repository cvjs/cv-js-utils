/**
 * @title geo地址区域
 */

/**
 * @action 计算位置距离
 * @param to_latitude 对象 纬度
 * @param to_longitude 对象 经度
 * @param my_latitude 当前我的 纬度
 * @param my_longitude 当前我的 经度
 * @returns {Number}
 */
function geoCalcDistance(to_lat, to_lng, my_lat, my_lng, unit = '') {
  if (to_lat == '' || to_lng == '') return '未知';
  var rad_to_lat = (to_lat * Math.PI) / 180.0;
  var rad_my_lat = (my_lat * Math.PI) / 180.0;
  var a = rad_to_lat - rad_my_lat;
  var b = (to_lng * Math.PI) / 180.0 - (my_lng * Math.PI) / 180.0;
  var s =
    2 *
    Math.asin(
      Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(rad_to_lat) * Math.cos(rad_my_lat) * Math.pow(Math.sin(b / 2), 2))
    );
  s = s * 6378.137; // EARTH_RADIUS;
  s = s.toFixed(2);
  switch (unit.toLowerCase()) {
    case 'km':
      s = s + 'km';
      break;
    case 'm':
      s = s * 1000;
      s = s < 100 ? '<100m' : s + 'm';
      break;
    default:
      if (s > 1) {
        s = s + 'km';
      } else {
        s = s * 1000;
        s = s < 100 ? '<100m' : s + 'm';
      }
  }
  return s;
}

export { geoCalcDistance };
