/**
 * @title 检测设备驱动
 */

/**
 * @action 是否桌面端
 * @param {*} UA
 * @returns {Boolean}
 */
function isDevice_desktop(UA) {
  let userAgent = UA || (typeof window !== 'undefined' && window.navigator.userAgent);
  return !userAgent.match(
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
  );
}

/**
 * @action 是否 移动端
 * @param {*} UA
 * @returns {Boolean}
 */
function isDevice_mobile(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = !!userAgent.match(/AppleWebKit.*Mobile.*/);
  return exactness;
  return /(Android|webOS|iPhone|iPod|tablet|BlackBerry|Mobile)/i.test(userAgent) ? true : false;
}
/**
 * @action 是否 IOS
 * @param {*} UA
 * @returns {Boolean}
 */
function isDevice_ios(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = !!userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/) || /iphone|ipad|ipod/i.test(userAgent);
  return exactness;
  // let ua = typeof window !== 'undefined' && window.navigator.userAgent.toLowerCase();
  // return ua && /iphone|ipad|ipod|ios/.test(ua);
  // return /iphone|ipad|ipod/gi.test(userAgent);
  // return /iPhone|iPad|iPod/i.test(userAgent) ? true : false;
}

/**
 * @action 是否 iphone
 * @desc 是否为iPhone或者QQHD浏览器
 * @param {*} UA
 * @returns {Boolean}
 */
function isDevice_iphone(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('iPhone') > -1;
  return exactness;
}
/**
 * @action 是否 iPad
 * @param {*} UA
 * @returns {Boolean}
 */
function isDevice_ipad(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('iPad') > -1;
  return exactness;
}

/**
 * @action 是否 andorid
 * @param {*} UA
 * @returns {Boolean}
 */
function isDevice_android(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('Android') > -1 || userAgent.indexOf('Adr') > -1;
  return exactness;
  return /Android/i.test(userAgent) ? true : false;
  let ua = typeof window !== 'undefined' && window.navigator.userAgent.toLowerCase();
  return ua && ua.indexOf('android') > 0;
}
/**
 * @action 是否 有网络权限
 * @param {*} UA
 * @returns {Boolean}
 */
function hasDevice_internet() {}

export { isDevice_desktop, isDevice_mobile, isDevice_ios, isDevice_iphone, isDevice_ipad, isDevice_android, hasDevice_internet };
