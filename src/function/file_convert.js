/**
 * @title 文件转换
 */
// https://blog.csdn.net/aexwx/article/details/88310207
// https://www.jianshu.com/p/85f621a5b948

/**
 * @action dataUrl to Blob
 * base64位码转blob对象
 * 把base64位的toDataURL图片转换成blob
 */
function dataURL_to_blob(dataurl) {
  var arr = dataurl.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
}
/**
 * @action blob to dataURL
 */
function blob_to_dataURL(blob, callback) {
  var a = new FileReader();
  a.onload = function (e) {
    callback(e.target.result);
  };
  a.readAsDataURL(blob);
}

/**
 * @action 将base64/dataurl转成File
 * @param dataurl
 * @param filename
 * @returns {File}
 */
function dataURL_to_file(dataurl, filename) {
  var arr = dataurl.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, { type: mime });
}

/**
 * @action 将base64转成blob
 * @param {*} param0
 * @returns
 */
function base64_to_blob({ b64data = '', contentType = '', sliceSize = 512 } = {}) {
  return new Promise((resolve, reject) => {
    // 使用 atob() 方法将数据解码
    let byteCharacters = atob(b64data);
    let byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);
      let byteNumbers = [];
      for (let i = 0; i < slice.length; i++) {
        byteNumbers.push(slice.charCodeAt(i));
      }
      // 8 位无符号整数值的类型化数组。内容将初始化为 0。
      // 如果无法分配请求数目的字节，则将引发异常。
      byteArrays.push(new Uint8Array(byteNumbers));
    }
    let result = new Blob(byteArrays, {
      type: contentType
    });
    result = Object.assign(result, {
      // jartto: 这里一定要处理一下 URL.createObjectURL
      preview: URL.createObjectURL(result),
      name: `图片示例.png`
    });
    resolve(result);
  });
}

/**
 * @action base 64 转 blob url
 * @param base_64
 * @return
 */
function base64_to_blobURL(base_64) {
  var blobObj = dataURL_to_blob(base_64);
  var blobUrl = '';
  blob_to_dataURL(blobObj, function (dataurl) {
    blobUrl = dataurl;
  });
  return blobUrl;
}
/**
 * @action 获取File 对象或 Blob 对象的临时路径
 * @param {*} file
 * @returns
 */
function getObjectURL(file) {
  let url = null;
  if (window.createObjectURL) {
    // basic
    url = window.createObjectURL(file);
  } else if (window.URL) {
    // mozilla(firefox)
    url = window.URL.createObjectURL(file);
  } else if (window.webkitURL) {
    // webkit or chrome
    url = window.webkitURL.createObjectURL(file);
  }
  return url;
}

export { dataURL_to_blob, blob_to_dataURL, base64_to_blob, base64_to_blobURL, base64_to_path };
