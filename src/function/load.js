import { strRightExists } from './str_common.js';
/**
 * @title 加载
 */

let __load_script_promise = {};
let __load_css_promise = {};
let __load_iframe_promise = {};
/**
 * @action Promise动态加载css
 * @param url
 * @returns {Promise}
 */
function loadCssPromise(url) {
  return new Promise(async (resolve, reject) => {
    // url = $A.originUrl(url);
    //
    let i = 0;
    while (__load_css_promise[url] === 'loading') {
      await new Promise((r) => setTimeout(r, 1000));
      i++;
      if (i > 30) {
        return reject('加载超时');
      }
    }
    if (__load_css_promise[url] === 'loaded') {
      return resolve(false);
    }
    __load_css_promise[url] = 'loading';
    //
    const script = document.createElement('link');
    if (script.readyState) {
      script.onreadystatechange = () => {
        if (script.readyState == 'loaded' || script.readyState == 'complete') {
          script.onreadystatechange = null;
          __load_css_promise[url] = 'loaded';
          resolve(true);
        }
      };
    } else {
      script.onload = () => {
        __load_css_promise[url] = 'loaded';
        resolve(true);
      };
      script.onerror = (e) => {
        __load_css_promise[url] = 'error';
        reject(e);
      };
    }
    script.rel = 'stylesheet';
    if (strRightExists(url, '.css')) {
      script.href = url + '?hash=' + window.systemInfo.version;
    } else {
      script.href = url;
    }
    document.getElementsByTagName('head').item(0).appendChild(script);
  });
}
/**
 * @action 异步加载js
 */
function loadScript(url, callback) {
  let script = document.createElement('script');
  script.type = 'text/javascript';
  if (script.readyState) {
    script.onreadystatechange = () => {
      if (script.readyState === 'loaded' || script.readyState === 'complete') {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {
    script.onload = () => {
      callback();
    };
  }
  script.src = url;
  document.body.appendChild(script);
}

/**
 * @action 同步加载js
 * @returns
 */
function loadScriptSync() {
  return true;
}

/**
 * @action Promise动态加载js
 * @returns {Promise}
 */
function loadScriptPromise(url) {
  return new Promise(async (resolve, reject) => {
    // url = originUrl(url);
    let i = 0;
    while (__load_script_promise[url] === 'loading') {
      await new Promise((r) => setTimeout(r, 1000));
      i++;
      if (i > 30) {
        return reject('加载超时');
      }
    }
    if (__load_script_promise[url] === 'loaded') {
      return resolve(false);
    }
    __load_script_promise[url] = 'loading';
    //
    const script = document.createElement('script');
    script.type = 'text/javascript';
    if (script.readyState) {
      script.onreadystatechange = () => {
        if (script.readyState === 'loaded' || script.readyState === 'complete') {
          script.onreadystatechange = null;
          __load_script_promise[url] = 'loaded';
          resolve(true);
        }
      };
    } else {
      script.onload = () => {
        __load_script_promise[url] = 'loaded';
        resolve(true);
      };
      script.onerror = (e) => {
        __load_script_promise[url] = 'error';
        reject(e);
      };
    }
    if (strRightExists(url, '.js')) {
      // script.src = url + '?hash=' + window.systemInfo.version;
      script.src = url + '?t=' + new Date().getTime();
    } else {
      script.src = url;
    }
    document.body.appendChild(script);
  });
}
/**
 * @action 动态加载iframe
 * @param url
 * @param loadedRemove
 * @returns {Promise}
 */
function loadIframePromise(url, loadedRemove = 0) {
  return new Promise(async (resolve, reject) => {
    url = $A.originUrl(url);
    //
    let i = 0;
    while (__load_iframe[url] === 'loading') {
      await new Promise((r) => setTimeout(r, 1000));
      i++;
      if (i > 30) {
        return reject('加载超时');
      }
    }
    if (__load_iframe[url] === 'loaded') {
      return resolve(false);
    }
    __load_iframe[url] = 'loading';
    //
    const iframe = document.createElement('iframe');
    iframe.style.display = 'none';
    iframe.src = url;
    iframe.onload = () => {
      __load_iframe[url] = 'loaded';
      resolve(true);
      if (loadedRemove > 0) {
        setTimeout(() => {
          document.body.removeChild(iframe);
          delete __load_iframe[url];
        }, loadedRemove);
      }
    };
    iframe.onerror = (e) => {
      __load_iframe[url] = 'error';
      reject(e);
    };
    document.body.appendChild(iframe);
  });
}
export { loadCssPromise, loadScript, loadScriptSync, loadScriptPromise, loadIframePromise };
