/**
 * @title 检测字符串
 */

/**
 * @action 是否 英文
 * @param {String} str
 * @returns {Boolean}
 */
function isStrEN(val) {
  return /^[\u4e00-\u9fa5]+[\u00b7\.]?[\u4e00-\u9fa5]+$/.test(val);
}
/**
 * @action 是否 全中文
 * @desc 只允许中文
 * @param {String} str
 * @returns {Boolean}
 */
function isStrZh(val) {
  return /[\u4e00-\u9fa5]+$/.test(val);
}
/**
 * @action 是否 中文+英文
 * @param {String} str
 * @returns {Boolean}
 */
function isStrZhOrEn(str) {
  return /^[0-9a-zA-Z\u4e00-\u9fa5_-]+$/.test(str);
}

/**
 * @action 某个字符是否是汉字
 * @param {String} str
 * @returns {Boolean}
 */
function isStrCHS(oldStr, i) {
  if (oldStr.charCodeAt(i) > 255 || oldStr.charCodeAt(i) < 0) {
    return true;
  } else {
    return false;
  }
}
/**
 * @action 只有中文和英文
 * @param {String} str
 * @returns {Boolean}
 */
function isStrAbroad(val) {
  return /^[a-zA-Z\u4e00-\u9fa5]+([\u00b7\.\- ]?[a-zA-Z\u4e00-\u9fa5]+)*$/.test(val);
}
/**
 * @action 只有英文和数字
 * @param {String} str
 * @returns {Boolean}
 */
// 英文和数字
function isStrEnNum(val) {
  return /^[0-9A-Za-z]+$/.test(val);
}

export { isStrZhOrEn, isStrAbroad, isStrEN, isStrZh, isStrCHS, isStrEnNum };
