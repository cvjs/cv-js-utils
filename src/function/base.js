/**
 * @title 基础
 */

/**
 * @action 函数节流
 * @desc 函数节流 (节流阀的意思,大致意思时节约触发的频率)
 * 用处:多用于页面scroll滚动,或者窗口resize,或者防止按钮重复点击等情况
 * @param {Function} func 执行函数
 * @param {*}  delay 节流时间,毫秒.默认500
 */
function baseThrottle(func, delay) {
  let last,
    timer = null;
  delay = delay || 500;
  return function () {
    var args = arguments,
      now = +new Date();
    if (last && now - last < delay) {
      clearTimeout(timer);
      timer = setTimeout(() => {
        last = now;
        func.apply(this, args);
      }, delay);
    } else {
      last = now;
      func.apply(this, args);
    }
  };
}

// // 函数节流 节流函数fn
// baseThrottle(fn, 500);
// baseThrottle(() => {
//   console.log('每500毫秒执行一次');
// }, 500);

/**
 * @action 函数防抖
 * (只执行最后一次点击,防止重复触发) 真正的含义就是:延迟函数执行,不管触发几次多久,只在最后一次触发时 用处:多用于 input 框输入时,显示匹配的输入内容的情况
 * @param {Object}  func 执行函数
 * @param {*} wait 防抖时间，毫秒，默认500
 */
function baseDebounce(func, wait) {
  var timer = null;
  wait = wait || 500;
  return function () {
    var args = arguments;
    if (timer !== null) clearTimeout(timer);
    timer = setTimeout(() => {
      timer = null;
      func.apply(this, args);
    }, wait);
  };
}

/**
 * @action 相当于 intval
 * @param str
 * @param fixed
 * @returns {Number}
 */
function parseNumber(str, fixed = null) {
  let _s = Number(str);
  if (_s + '' === 'NaN') {
    _s = 0;
  }
  if (fixed && /^[0-9]*[1-9][0-9]*$/.test(fixed)) {
    _s = _s.toFixed(fixed);
    let rs = _s.indexOf('.');
    if (rs < 0) {
      _s += '.';
      for (let i = 0; i < fixed; i++) {
        _s += '0';
      }
    }
  }
  return _s;
}

export { baseThrottle, baseDebounce, parseNumber };
