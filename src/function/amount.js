/**
 * @title 数字金额
 */

/**
 * @action 数字金额转中文
 * @param {number} amount 100
 * @returns {String} 一百元整
 */
function amountToChinese(amount = 0) {
  amount = parseFloat(amount);
  if (isNaN(amount)) {
    return;
  } // || Math.abs(amount) > 99999999999.99
  amount = Math.round(amount * 100);
  var isInt = amount % 100 == 0 ? true : false;
  var numArr = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
  var unitArr = ['分', '角', '元', '拾', '佰', '仟', '万', '拾', '佰', '仟', '亿', '拾', '佰', '仟'];
  var resultStr = '',
    num,
    unitIdx,
    len,
    zeroCount = 0;
  if (amount == 0) {
    return '零元整';
  }
  if (amount < 0) {
    resultStr += '负';
    amount = -amount;
  }
  amount = amount.toString();
  len = amount.length;
  for (var i = 0; i < len; i++) {
    num = parseInt(amount.charAt(i));
    unitIdx = len - 1 - i;
    if (num == 0) {
      //元 万 亿 输出单位
      if (unitIdx == 2 || unitIdx == 6 || unitIdx == 11) {
        resultStr += unitArr[unitIdx];
        zeroCount = 0;
      } else {
        zeroCount++;
      }
    } else {
      if (zeroCount > 0) {
        resultStr += '零';
        zeroCount = 0;
      }
      resultStr = resultStr + numArr[num] + unitArr[unitIdx];
    }
  }
  if (isInt) {
    resultStr += '整';
  }
  return resultStr;
}

/**
 * @action 将数字\金额转成3位分隔符
 * @param {*} num
 * @returns {Number}
 */
function amountSplit3(num) {
  if (num === null || num === undefined) {
    return null;
  }
  if (typeof num === 'number') {
    num = num.toString();
  }
  return num.replace(/\B(?=(?:\d{3})+\b)/g, ',');
}

export { amountToChinese, amountSplit3 };
