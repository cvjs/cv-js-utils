/**
 * @title 文件操作
 */

import { parseNumber } from './base.js';
/**
 * @action 获取文件后缀
 * @param {*} u
 * @returns {String}
 */
function fileGetExt(u) {
  // 处理后缀
  var us, rs;
  if (u.indexOf('?') > -1) {
    us = u.split('?');
    u = us[0];
  } else if (u.indexOf('#') > -1) {
    us = u.split('#');
    u = us[0];
  }
  us = u.split('/');
  u = us[us.length - 1];
  us = u.lastIndexOf('.');
  rs = us > -1 ? u.substr(us + 1).toLowerCase() : '';
  return rs;
}
/**
 * @action 验证文件是否图片
 * @param {*} src
 * @returns {Boolean}
 */
function fileCheckImg(src) {
  var ext = src == '' ? '' : fileGetExt(src);
  return ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'bmp' ? 1 : 0;
}

/**
 * @action 压缩图片
 * @param {*} dataUrl
 * @param {*} obj：{ height, width } 压缩之后的图片宽高
 * @param {*} type 压缩完之后的图片类型
 * @returns {Promise}
 */
function fileImgCompress(dataUrl, obj, type = 'image/png') {
  return new Promise((resolve, reject) => {
    let img = new Image();
    img.src = dataUrl;
    img.onload = function () {
      let that = this;
      // 默认按比例压缩
      let w = that.width,
        h = that.height,
        scale = w / h;
      if (h >= obj.height) {
        h = obj.height;
      }
      w = h * scale;

      let quality = 1; // 默认图片质量为1
      //生成canvas
      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');
      // 创建属性节点
      let anw = document.createAttribute('width');
      anw.nodeValue = w;
      let anh = document.createAttribute('height');
      anh.nodeValue = h;
      canvas.setAttributeNode(anw);
      canvas.setAttributeNode(anh);
      ctx.drawImage(that, 0, 0, w, h);
      // 图像质量
      if (obj.quality && obj.quality <= 1 && obj.quality > 0) {
        quality = obj.quality;
      }
      // quality值越小，所绘制出的图像越模糊
      let base64 = canvas.toDataURL(type, quality);
      // 回调函数返回base64的值
      resolve(base64);
    };
    img.onerror = () => {
      reject();
    };
  });
}

/**
 * @action 字节转换
 * @param {Int } bytes
 * @returns {String}
 */
function fileBytesToSize(bytes) {
  if (bytes === 0) return '0 B';
  let k = 1024;
  let sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  let i = Math.floor(Math.log(bytes) / Math.log(k));
  if (typeof sizes[i] === 'undefined') {
    return '0 B';
  }
  return parseNumber(bytes / Math.pow(k, i), 2) + ' ' + sizes[i];
}
export { fileGetExt, fileCheckImg, fileImgCompress, fileBytesToSize };
