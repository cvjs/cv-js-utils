/**
 * 数组
 */
export * from './function/array_base';
export * from './function/array_tree';
/**
 * 验证
 */
export * from './function/check_browser';
export * from './function/check_common';
export * from './function/check_device';
export * from './function/check_num';
export * from './function/check_str';
export * from './function/check_type';
export * from './function/check_idcard';
/**
 * 字符串
 */
export * from './function/str_common';
export * from './function/str_replace';
/**
 * 其他
 */
export * from './function/amount'; // 金额
export * from './function/base'; // 基础
export * from './function/color'; // 颜色
export * from './function/file_common'; // 文件
export * from './function/geo'; // 地理geo
export * from './function/number'; // 数字
export * from './function/object'; // 对象
export * from './function/random'; // 随机
export * from './function/uri'; // uri
export * from './function/copy';
export * from './function/datetime'; // 时间日期
export * from './function/load';

/**
 * 类
 */
export * from './class/LibsDate';

export {};

function setupCounter(element) {
  let counter = 0;
  const setCounter = (count) => {
    counter = count;
    element.innerHTML = `count is ${counter}`;
  };
  element.addEventListener('click', () => setCounter(++counter));
  setCounter(0);
}
export { setupCounter };
