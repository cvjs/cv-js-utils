/**
 * 会话存储-存   -------------------------------------------------------会话存储------------------------------------
 * @param {*} key 键名
 * @param {*} value 值
 */
function sessionSet(key, value) {
  sessionStorage.setItem(key, JSON.stringify(value));
}

/**
 * 会话存储-取
 * @param {*} key 键名
 */
function sessionGet(key) {
  let _data = sessionStorage.getItem(key);
  try {
    return JSON.parse(_data);
  } catch (e) {
    return _data;
  }
}
class StorageClass {
  // 设置缓存
  setCacheData(name, val, cacheDay) {
    // 判断是否支持或开启localStorage
    // 无痕模式下和ie安全模式下localStorage会被禁用
    if (localStorage) {
      localStorage.setItem(name, val);
    } else {
      this.set(name, val, cacheDay || 1000);
    }
  }
  // 获取缓存
  getCacheData(name) {
    if (localStorage) {
      return localStorage.getItem(name);
    } else {
      return this.get(name);
    }
  }
  // 清除缓存
  removeCacheData(name) {
    if (localStorage) {
      localStorage.removeItem(name);
    } else {
      this.remove(name);
    }
  }
}
/**
 * 会话存储-删
 * @param {*} key 键名
 */
function sessionDel(key) {
  sessionStorage.removeItem(key);
}

/**
 * 会话存储-删处全部
 */
function sessionClear() {
  sessionStorage.clear();
}

/**
 * 永久存储-存  -------------------------------------------------------永久存储-----------------------------------
 * @param {*} key 键名
 * @param {*} value 值
 */
/**
 * @description localStorage set
 * @param key 只支持 字符串和数字
 * @param val 支持除了Function和Symbol的所有数据类型
 */
function localSet(key, value) {
  if (!window.localStorage) {
    console.warn('浏览器不支持localStorage');
    return;
  }
  window.localStorage.setItem(key, JSON.stringify(value));
}

/**
 * 永久存储-取
 * @param {*} key 键名
 */
function localGet(key) {
  if (!window.localStorage) {
    console.warn('浏览器不支持localStorage');
    return;
  }
  let _data = window.localStorage.getItem(key);
  try {
    if (Object.keys(_data).length == 0 || JSON.stringify(_data) == '{}') {
      _data = '';
    } else {
      // 取缓存,如果解析出来的是对象, 则返回对象
      _data = JSON.parse(_data);
    }
  } catch (e) {
    // console.warn("val不是对象或者数组");
  }
  return _data;
}

/**
 * 永久存储-删
 * @description 移除某一个key的storage
 * @param {*} key 键名
 */
function localDel(key) {
  if (!window.localStorage) return;
  window.localStorage.removeItem(key);
}

export { sessionSet, sessionGet, sessionDel, sessionClear, localSet, localGet, localDel };
