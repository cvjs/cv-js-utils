class LibsCookieClass {
  constructor() {
    this._expires = '';
    return this;
  }
  setExpires() {}
  // 设置cookie
  setCookie(name, value, attributes) {
    let expires, path;
    if (attributes) {
      expires = attributes.expires || 0;
      path = attributes.path || '/';
    }
    let date = Date.now() + expires * 24 * 60 * 60 * 1000; // cookie过期时间
    date = new Date(date).toUTCString();
    document.cookie = name + '=' + encodeURIComponent(value) + (!expires ? '' : '; expires=' + date) + ';path=' + path + ';';
  }
  // 批量设置cookie
  setCookies(obj) {
    for (let i = 0; i < obj.length; i++) {
      this.setCookie(obj[i][0], obj[i][1], obj[i][2]);
    }
  }
  // 获取cookie
  getCookie(name) {
    if (document.cookie.length > 0) {
      let start = document.cookie.indexOf(name + '=');
      if (start !== -1) {
        start = start + name.length + 1;
        let end = document.cookie.indexOf(';', start);
        if (end === -1) {
          end = document.cookie.length;
        }
        return decodeURIComponent(document.cookie.substring(start, end)); /* 获取解码后的cookie值 */
      }
    }
    return null;
  }
  // 清除特定cookie
  delCookie(name) {
    this.setCookie(name, '', -1);
  }
  // 批量清除cookie
  clearCookies(obj) {
    for (let i = obj.length - 1; i >= 0; i--) {
      this.remove(obj[i]);
    }
    this.setCookie(name, '', -1);
  }
  // 清除所有cookie
  clearAllCookie() {
    let keys = document.cookie.match(/[^ =;]+(?=\=)/g);
    if (keys) {
      for (let i = keys.length; i--; ) {
        document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString();
      }
    }
  }
}
/**
 * 导出方法
 */
export default function LibsCookie() {
  return new LibsCookieClass();
}
// 写cookies
function ctoCookieSet(name, value, days) {
  days = days || 30;
  var expires = '';
  // days = uUtils.isComEmpty(day) ? 30 : days;
  var date = new Date();
  date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
  // expires = "; expires=" + date.toGMTString();
  // document.cookie = name + "=" + value + expires + "; path=/";
  document.cookie = name + '=' + escape(value) + ';expires=' + date.toGMTString();
}
function ctoCookieGet3(name) {
  var arg = name + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(arg) == 0) return c.substring(arg.length, c.length);
  }
  return null;
}
// read a cookie
function ctoCookieGet2(name) {
  var arg = name + '=';
  var alen = arg.length;
  var clen = document.cookie.length;
  var i = 0;
  while (i < clen) {
    var j = i + alen;
    if (document.cookie.substring(i, j) == arg) {
      var endstr = document.cookie.indexOf(';', j);
      if (endstr == -1) {
        endstr = document.cookie.length;
      }
      var ret = unescape(document.cookie.substring(j, endstr));
      if (ret != '') {
        return ret;
      }
    }
    i = document.cookie.indexOf(' ', i) + 1;
    if (i == 0) break;
  }
  return '';
}
function ctoCookieGet5(name, key) {
  var nameValue = '';
  var arr,
    reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)');
  if ((arr = document.cookie.match(reg))) {
    nameValue = UrlDecodeGB(arr[2]);
  }
  if (key != null && key != '') {
    reg = new RegExp('(^| |&)' + key + '=([^(;|&|=)]*)(&|$)');
    if ((arr = nameValue.match(reg))) {
      return UrlDecodeGB(arr[2]);
    } else return nameValue;
  } else {
    return nameValue;
  }
}

function ctoCookieGetBySubName(sMainName, sSubName) {
  var re = new RegExp(sSubName ? sMainName + '=(?:.*?&)*?' + sSubName + '=([^&;$]*)' : sMainName + '=([^;$]*)', 'i');
  return re.test(unescape(document.cookie)) ? RegExp['$1'] : '';
}

// add a cookie
function ctoCookieAdd(name, value, expireHours) {
  var cookieString = name + '=' + escape(value) + '; path=/';
  // 判断是否设置过期时间
  if (expireHours > 0) {
    var date = new Date();
    date.setTime(date.getTime() + expireHours * 3600 * 1000);
    cookieString = cookieString + ';expires=' + date.toGMTString();
  }
  document.cookie = cookieString;
}
function ctoCookieAdd2(name, cookieValue) {
  var expDate = new Date();
  var argv = addCookie.arguments;
  var argc = addCookie.arguments.length;
  //
  var expires = argc > 2 && argv[2] != 0 ? argv[2] : null;
  var ckExpires = '';
  if (expires != null) {
    expDate.setTime(expDate.getTime() + expires * 1000);
    ckExpires = '; expires=' + expDate.toGMTString();
  }
  var path = argc > 3 ? argv[3] : null;
  var ckPath = path == null ? '' : '; path=' + path;
  var domain = argc > 4 ? argv[4] : null;
  var ckDomain = domain == null ? '' : '; domain=' + domain;
  var secure = argc > 5 ? argv[5] : false;
  var ckSecure = secure == true ? '; secure' : '';

  document.cookie = name + '=' + escape(cookieValue) + ckExpires + ckPath + ckDomain + ckSecure;
}
function delCookie() {
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval = ctoCookieGet(name);
  domain = domain || '';
  if (cval != null) {
    document.cookie = name + '=' + cval + ';expires=' + exp.toGMTString();
    // document.cookie = name + "=; path=/; expires=" + exp.toGMTString();
    // if (domain != '') {
    // document.cookie = name + "=; path=/; domain=" + domain + "; expires=" + exp.toGMTString();
    // }
  }
}
function ctoCookieSet2(name, value, days) {
  days = days || 30;
  if (days) {
    var exp = new Date();
    exp.setTime(exp.getTime() + days * 24 * 60 * 60 * 1000);

    var expires = '; expires=' + date.toGMTString();
  } else var expires = '';
  var arr = document.cookie.match(new RegExp('(^| )' + name + '=([^;]*)(;|$)'));
  document.cookie = name + '=' + escape(value) + ';expires=' + exp.toGMTString();
  document.cookie = name + '=' + value + expires + '; path=/';
}
