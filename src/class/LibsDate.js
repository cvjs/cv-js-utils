/**
 * 对Date的扩展，将 Date 转化为指定格式的String
 * 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
 * 例子：
 * (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
 * (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
 */
const _diyFormatDate = function (date, fmt) {
  const opt = {
    'y+': date.getFullYear().toString(), // 年
    'M+': (date.getMonth() + 1).toString(), // 月份
    'd+': date.getDate().toString(), // 日
    'h+': date.getHours().toString(), // 小时
    'm+': date.getMinutes().toString(), // 分
    's+': date.getSeconds().toString(), // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    S: date.getMilliseconds(), // 毫秒
    'w+': date.getDay(), // 星期
    'a+': date.getDay(), // 星期
    W: date.getDay(), // 星期
    GMT: date
  };
  /**
   * 旧的写法
   */
  // if (/(y+)/.test(fmt)) {
  //   fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  // }
  // for (var k in o) {
  //   if (new RegExp('(' + k + ')').test(fmt)) {
  //     fmt = fmt.replace(ret[1], ret[1].length === 1 ? opt[k] : ('00' + o[k]).substr(('' + o[k]).length));
  //   }
  // }
  /**
   * 新的
   */
  for (const k in opt) {
    if (k == 'W') {
      if (opt[k] == 0) opt[k] = '日';
      if (opt[k] == 1) opt[k] = '一';
      if (opt[k] == 2) opt[k] = '二';
      if (opt[k] == 3) opt[k] = '三';
      if (opt[k] == 4) opt[k] = '四';
      if (opt[k] == 5) opt[k] = '五';
      if (opt[k] == 6) opt[k] = '六';
    }
    const ret = new RegExp('(' + k + ')').exec(fmt);
    if (ret) {
      fmt = fmt.replace(ret[1], ret[1].length === 1 ? opt[k] : opt[k].padStart(ret[1].length, '0'));
    }
  }
  return fmt;
};
/**
 *
 * @
 */
export class LibsDateClass {
  constructor() {
    let dateObj = new Date();
    this.dateObj = dateObj;
    this._formatType = 'yyyy-MM-dd hh:mm:ss';
    return this;
  }
  setYear(setVal = 0) {
    if (setVal == 0 || setVal == '') {
    } else {
      this.dateObj.setFullYear(this.dateObj.getFullYear() + setVal);
    }
    return this;
  }
  setMonth(setVal = 0) {
    if (setVal == 0 || setVal == '') {
    } else {
      this.dateObj.setMonth(this.dateObj.getMonth() + setVal);
    }
    return this;
  }
  setDay(setVal = 0) {
    if (setVal == 0 || setVal == '') {
    } else {
      this.dateObj.setDate(this.dateObj.getDate() + setVal);
    }
    return this;
  }
  /**
   * 设置时间戳
   */
  setTime(timeSitamp) {
    timeSitamp = timeSitamp || '';
    if (timeSitamp != '') {
      if (timeSitamp.toString().length == 10) {
        // 如果date为13位不需要乘1000,[js时间戳长度是13位]，php，java等时间戳长度为10位
        timeSitamp = parseInt(timeSitamp) * 1000;
      }
      // parseInt(timeSitamp)
      this.dateObj.setTime(timeSitamp);
    }
    return this;
  }
  init(timeOrDate) {
    if (Object.prototype.toString.call(timeOrDate) == '[object Number]') {
      this.setTime(timeOrDate);
    } else if (
      Object.prototype.toString.call(timeOrDate) == '[object Date]' ||
      Object.prototype.toString.call(timeOrDate) == '[object String]'
    ) {
      this.dateObj = new Date(timeOrDate);
    }
    return this;
  }
  format(formatStr = 'yyyy-MM-dd hh:mm:ss') {
    formatStr = formatStr || '';
    if (formatStr != '') {
      this._formatType = formatStr;
    }
    return this;
  }
  /** 返回结果为 string */

  _valToString() {
    return _diyFormatDate(this.dateObj, this._formatType);
  }
  /** 返回结果为 int */
  _valToInt() {
    let timeSitamp = this.dateObj.getTime();
    timeSitamp = parseInt(timeSitamp) / 1000;
    return timeSitamp;
  }
  /** 返回结果为 object */
  _valToObject() {
    let last_arr = {
      year: this.dateObj.getFullYear(), //年
      month: this.dateObj.getMonth() + 1, //月
      day: this.dateObj.getDate(), //日
      hour: this.dateObj.getHours(), //时
      minute: this.dateObj.getMinutes(), //分
      second: this.dateObj.getSeconds(), //秒
      millisecond: this.dateObj.getMilliseconds(), //秒
      quarter: Math.floor((this.dateObj.getMonth() + 3) / 3), // 季度
      week: this.dateObj.getDay(), // 星期
      week_cn: this.dateObj.getDay() // 中文星期
    };
    for (let key in last_arr) {
      let item = last_arr[key];
      if (key != 'y') {
        if (key === 'a' || key == 'week') {
          last_arr[key] = item;
        } else if (key === 'week_cn') {
          if (item == 0) last_arr[key] = '日';
          if (item == 1) last_arr[key] = '一';
          if (item == 2) last_arr[key] = '二';
          if (item == 3) last_arr[key] = '三';
          if (item == 4) last_arr[key] = '四';
          if (item == 5) last_arr[key] = '五';
          if (item == 6) last_arr[key] = '六';
        } else {
          last_arr[key] = String(item < 10 ? '0' + item : item);
        }
      }
    }
    return last_arr;
  }
  _valToArray() {}
  value(type) {
    type = type || 'string';
    let lastVal;
    if (type == 'string') {
      // 日期格式
      lastVal = this._valToString();
    } else if (type == 'object') {
      // 转对象
      lastVal = this._valToObject();
    } else if (type == 'int') {
      // 转时间戳
      lastVal = this._valToInt();
    }
    return lastVal;
  }
}
/**
 * 导出方法
 */
export function LibsDate() {
  return new LibsDateClass();
}
