let formatText = {
  weekArr: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'],
  daysArr: ['日', '一', '二', '三', '四', '五', '六', '日'],
  months_zh: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
  months_num: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
  months_short: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
  today: '今天',
  clear: '清空'
};

/**
 * 比较时间
 *@param {String} timeOne  时间 1
 * @param {String} timeTwo  时间 2
 */
function timeCompareStr(timeOne, timeTwo) {
  // 判断 timeOne 和 timeTwo 是否
  return new Date(timeOne.replace(/-/g, '/')).getTime() < new Date(timeTwo.replace(/-/g, '/')).getTime();
}

function timeCompareStr2(a, b) {
  var arr = a.split('-');
  var starttime = new Date(arr[0], arr[1], arr[2]);
  var starttimes = starttime.getTime();
  var arrs = b.split('-');
  var lktime = new Date(arrs[0], arrs[1], arrs[2]);
  var lktimes = lktime.getTime();
  if (starttimes >= lktimes) {
    return true;
  } else {
    return false;
  }
}
function timeToDiffer() {}

/**
 * 获取N久之前的时间
 * @author joyceZhang 迁移
 * @param {Number} time 时间戳
 *
 * @returns {String}
 */
const helperTimeFilter = (settT) => {
  let currT = parseInt(new Date().getTime() / 1000); //当前时间戳
  let diffT = currT - settT; // 参数时间戳与当前时间戳相差秒数

  let curDate = new Date(currT * 1000); // 当前时间日期对象
  let tmDate = new Date(settT * 1000); // 参数时间戳转换成的日期对象
  let nowArr = LibsDate().init(curDate).value('object');
  if (diffT < 60) {
    // 一分钟以内
    return '刚刚';
  } else if (diffT < 3600) {
    // 一小时前之内
    return Math.floor(diffT / 60) + '分钟前';
  } else if (curDate.getFullYear() == Y && curDate.getMonth() + 1 == m && curDate.getDate() == d) {
    return '今天' + nowArr.h + ':' + nowArr.i;
  } else {
    let newDate = new Date((currT - 86400) * 1000); // 参数中的时间戳加一天转换成的日期对象
    if (newDate.getFullYear() == Y && newDate.getMonth() + 1 == m && newDate.getDate() == d) {
      return '昨天' + nowArr.h + ':' + nowArr.i;
    } else if (curDate.getFullYear() == Y) {
      return helperAppendZero(m) + '月' + helperAppendZero(d) + '日 ' + nowArr.h + ':' + nowArr.i;
    } else {
      return Y + '年' + helperAppendZero(m) + '月' + helperAppendZero(d) + '日 ' + nowArr.h + ':' + nowArr.i;
    }
  }
};

function timeHandle(dateStr, type) {
  let nowArr = LibsDate().value('object');
  var nowTime = new Date().getTime() / 1000, // 获取此时此刻日期的秒数
    nowDate = new Date(), // 获取此时此刻日期
    diffValue = nowTime - publishTime, // 获取此时 秒数 与 要处理的日期秒数 之间的差值
    diff_days = parseInt(diffValue / 86400), // 一天86400秒 获取相差的天数 取整
    diff_hours = parseInt(diffValue / 3600), // 一时3600秒
    diff_minutes = parseInt(diffValue / 60),
    diff_secodes = parseInt(diffValue);

  if (type == 1 && diff_days <= 0 && nowDate.getDate() == parseInt(D)) return H + ':' + m;
  if (type == 1 && Y == nowDate.getFullYear()) return M + '-' + D + ' ' + H + ':' + m;
  if (type == 1) return Y + '-' + M + '-' + D + ' ' + H + ':' + m;
  if (diff_days > 0 && diff_days < 10) {
    // 相差天数 0 < diff_days < 3 时, 直接返出
    return diff_days + '天前';
  } else if (diff_days <= 0 && diff_hours > 0) {
    return diff_hours + '小时前';
  } else if (diff_hours <= 0 && diff_minutes > 0) {
    return diff_minutes + '分钟前';
  } else if (diff_secodes < 60) {
    if (diff_secodes <= 0) {
      return '刚刚';
    } else {
      return diff_secodes + '秒前';
    }
  } else if (diff_days >= 3 && diff_days < 30) {
    return M + '-' + D; // + ' ' + H + ':' + m;
  } else if (diff_days >= 30) {
    return Y + '-' + M + '-' + D; // + ' ' + H + ':' + m;
  }
}

/* 使用方法 */
// var hide_id = new Array();
// hide_id[0] = "huichangc";
// hide_id[1] = "huichangd";
// ctouiTimeCheck([ '9:00', '18:00' ], hide_id);
function ctouiTimeCheck(ar) {
  var currdata = new Date();
  t_year = currdata.getFullYear(); /* 年 */
  t_month = currdata.getMonth() + 1; /* 月 */
  t_day = currdata.getDate(); /* 日 */
  tz_month = (t_month < 10 ? '0' : '') + t_month;
  tz_day = (t_day < 10 ? '0' : '') + t_day;
  currdata1 = t_year + '-' + tz_month + '-' + tz_day;
  var current = currdata.getHours() * 60 + currdata.getMinutes();
  var ar_begin = ar[0].split(':');
  var ar_end = ar[1].split(':');
  var b = parseInt(ar_begin[0]) * 60 + parseInt(ar_begin[1]);
  var e = parseInt(ar_end[0]) * 60 + parseInt(ar_end[1]);

  if (timeCompareStr(currdata1, '2015-04-25') == true) {
    console.log(currdata1);
    if (current >= b && current < e) {
    } else if (current > e) {
      return false;
    }
  } else {
  }
}
/**
 * @param {Number} timeStamp 判断时间戳格式是否是毫秒
 * @returns {Boolean}
 */
const isMillisecond = (timeStamp) => {
  return String(timeStamp).length > 10;
};

/**
 * @param {Number} timeStamp 传入的时间戳
 * @param {Number} currentTime 当前时间时间戳
 * @returns {Boolean} 传入的时间戳是否早于当前时间戳
 */
const isEarly = (timeStamp, currentTime) => {
  return timeStamp < currentTime;
};

/**
 * @param {Number} num 数值
 * @returns {String} 处理后的字符串
 * @description 如果传入的数值小于10，即位数只有1位，则在前面补充0
 */
const getHandledValue = (num) => {
  return num < 10 ? '0' + num : num;
};

/**
 * @param {Number} timeStamp 传入的时间戳
 * @param {Number} startType 要返回的时间字符串的格式类型，传入'year'则返回年开头的完整时间
 */
const getDate2 = (timeStamp, startType) => {
  const d = new Date(timeStamp * 1000);
  const year = d.getFullYear();
  const month = getHandledValue(d.getMonth() + 1);
  const date = getHandledValue(d.getDate());
  const hours = getHandledValue(d.getHours());
  const minutes = getHandledValue(d.getMinutes());
  const second = getHandledValue(d.getSeconds());
  let resStr = '';
  if (startType === 'year') resStr = year + '-' + month + '-' + date + ' ' + hours + ':' + minutes + ':' + second;
  else resStr = month + '-' + date + ' ' + hours + ':' + minutes;
  return resStr;
};

/**
 * @param {String|Number} timeStamp 时间戳
 * @returns {String} 相对时间字符串
 */
export const getRelativeTime = (timeStamp) => {
  // 判断当前传入的时间戳是秒格式还是毫秒
  const IS_MILLISECOND = isMillisecond(timeStamp);
  // 如果是毫秒格式则转为秒格式
  if (IS_MILLISECOND) Math.floor((timeStamp /= 1000));
  // 传入的时间戳可以是数值或字符串类型，这里统一转为数值类型
  timeStamp = Number(timeStamp);
  // 获取当前时间时间戳
  const currentTime = Math.floor(Date.parse(new Date()) / 1000);
  // 判断传入时间戳是否早于当前时间戳
  const IS_EARLY = isEarly(timeStamp, currentTime);
  // 获取两个时间戳差值
  let diff = currentTime - timeStamp;
  // 如果IS_EARLY为false则差值取反
  if (!IS_EARLY) diff = -diff;
  let resStr = '';
  const dirStr = IS_EARLY ? '前' : '后';
  // 少于等于59秒
  if (diff <= 59) resStr = diff + '秒' + dirStr;
  // 多于59秒，少于等于59分钟59秒
  else if (diff > 59 && diff <= 3599) resStr = Math.floor(diff / 60) + '分钟' + dirStr;
  // 多于59分钟59秒，少于等于23小时59分钟59秒
  else if (diff > 3599 && diff <= 86399) resStr = Math.floor(diff / 3600) + '小时' + dirStr;
  // 多于23小时59分钟59秒，少于等于29天59分钟59秒
  else if (diff > 86399 && diff <= 2623859) resStr = Math.floor(diff / 86400) + '天' + dirStr;
  // 多于29天59分钟59秒，少于364天23小时59分钟59秒，且传入的时间戳早于当前
  else if (diff > 2623859 && diff <= 31567859 && IS_EARLY) resStr = getDate(timeStamp);
  else resStr = getDate(timeStamp, 'year');
  return resStr;
};
