const path = require('path');
const fs = require('fs');

function resolve(dir) {
  return path.join(__dirname, dir);
}

const port = process.env.VUE_APP_SYYY_PORT; // dev port
const dev = process.env.NODE_ENV === 'development';
const USER_HOME = process.env.HOME || process.env.USERPROFILE;

let proPublicPath = '';
let proConfWebpackPulgins = [];

/**
 * build oss
 */
// let buildOssPath = '/shiyun-devops/case/shiyun-webpack/domain-pk-pc.js';
let buildOssPath = '/shiyun-devops/case/shiyun-webpack/domain-pk-pc.js';
if (fs.existsSync(USER_HOME + buildOssPath)) {
  var buildOssConfig = require(USER_HOME + buildOssPath);
  var lastSavePath = require('../libs/utils').joinPathStr(process.env.VUE_APP_SY_APP_SIGN, true);
  console.log(lastSavePath);
  // var WebpackAliyunOss = require('../libs/aliyun-oss');
  let proPublicPath = buildOssConfig.OSS_URL + lastSavePath;
  // // 上传到阿里云
  // proConfWebpackPulgins.push(
  //   new WebpackAliyunOss(buildClass.setUpAliOss())
  // );
}
